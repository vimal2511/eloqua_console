﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace DAL
{

    public class DAL_Main : IDisposable
    {
        //SqlConnection Con = new SqlConnection(ConfigurationSettings.AppSettings["EloquaConfigCon"].ToString()); //changed
        SqlConnection Con;
        private IDbCommand cmd = new SqlCommand();
        private string strConnectionString = "";
        private bool handleErrors = false;
        private string strLastError = "";
        private CommandType _CommandType;

        public DAL_Main(string Key)
        {
           // string connStr = System.IO.File.ReadAllText("filename.txt");
            strConnectionString = ConfigurationSettings.AppSettings[Key].ToString();
            Con = new SqlConnection();
            Con.ConnectionString = strConnectionString;
            cmd.Connection = Con;
            _CommandType = CommandType.StoredProcedure;
            cmd.CommandType = _CommandType;
        }

        public CommandType commandType
        {
            get { return _CommandType; }
            set { _CommandType = value; }
        }

        #region ExcuteReader Functions

        public IDataReader ExecuteReader()
        {
            IDataReader reader = null;
            try
            {
                this.Open();
                reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception ex)
            {
                if (handleErrors)
                    strLastError = ex.Message;
                else
                    throw;
            }
            
            finally
            {
                this.Close();
            }
            return reader;
        }

        public IDataReader ExecuteReader(string commandtext)
        {
            IDataReader reader = null;
            try
            {
                cmd.CommandText = commandtext;
                reader = this.ExecuteReader();
            }
            catch (Exception ex)
            {
                if (handleErrors)
                    strLastError = ex.Message;
                else
                    throw;
            }
            
            finally
            {
                this.Close();
            }
            return reader;
        }
        #endregion

        #region ExecuteScalar Functions

        public object ExecuteScalar()
        {
            object obj = null;
            try
            {
                this.Open();
                cmd.CommandType = _CommandType;
                obj = cmd.ExecuteScalar();
                this.Close();
            }
            catch (Exception ex)
            {
                if (handleErrors)
                    strLastError = ex.Message;
                else
                    throw;
            }
            
            finally
            {
                this.Close();
            }
            return obj;
        }

        public object ExecuteScalar(string commnadtext)
        {
            object obj = null;
            try
            {
                cmd.CommandText = commnadtext;
                cmd.CommandType = _CommandType;
                obj = this.ExecuteScalar();
            }
            catch (Exception ex)
            {
                if (handleErrors)
                    strLastError = ex.Message;
                else
                    throw;
            }
            
            finally
            {
                this.Close();
            }
            return obj;
        }

        #endregion

        #region ExecuteNonQuery

        public int ExecuteNonQuery()
        {
            int i = -1;
            try
            {
                this.Open();
                i = cmd.ExecuteNonQuery();
                cmd.CommandTimeout = 200000;
                //this.Close();
            }
            catch (Exception ex)
            {
                if (handleErrors)
                    strLastError = ex.Message;
                else
                    throw;
            }
            
            finally
            {
                this.Close();
            }
            return i;
        }

        public int ExecuteNonQuery(string commandtext)
        {
            int i = -1;
            try
            {
                cmd.CommandText = commandtext;
                cmd.CommandType = _CommandType;
                cmd.CommandTimeout = 200000;
                i = this.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                if (handleErrors)
                    strLastError = ex.Message;
                else
                    throw;
            }
            
            finally
            {
                this.Close();
            }
            return i;
        }
        #endregion

        #region ExecuteDataSet Functions

        public DataSet ExecuteDataSet()
        {
            SqlDataAdapter da = null;
            DataSet ds = null;
            try
            {
                cmd.CommandType = _CommandType;
                cmd.CommandTimeout = 200000;
                da = new SqlDataAdapter();
                da.SelectCommand = (SqlCommand)cmd;
                if (da.SelectCommand.Connection.State == ConnectionState.Closed)
                    da.SelectCommand.Connection.Open();
                ds = new DataSet();
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                if (handleErrors)
                    strLastError = ex.Message;
                else
                    throw;
            }
            
            finally
            {
                this.Close();
            }
            return ds;
        }

        public DataSet ExecuteDataSet(string commandtext)
        {
            DataSet ds = null;
            try
            {
                cmd.CommandText = commandtext;
                cmd.CommandTimeout = 200000;
                ds = this.ExecuteDataSet();
            }
            catch (Exception ex)
            {
                if (handleErrors)
                    strLastError = ex.Message;
                else
                    throw;
            }
            
            finally
            {
                this.Close();
            }
            return ds;
        }
        #endregion

        #region ExecuteQuery Functions

        public void ExecuteQuery(string query, string result)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(query, Con);
                cmd.CommandTimeout = 200000;
                cmd.ExecuteNonQuery();
                result = "";
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
        }
        #endregion

        #region Execite sql Function
        public void Executesql(string query, DataTable dt, string result)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            try
            {
                da.SelectCommand = new SqlCommand(query, Con);
                da.SelectCommand.CommandTimeout = 200000;
                da.Fill(dt);
                result = "";
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
        }
        #endregion

        #region Connection String Functions
        public string CommandText
        {
            get { return cmd.CommandText; }
            set { cmd.CommandText = value; cmd.Parameters.Clear(); }
        }

        public IDataParameterCollection Parameters
        {
            get { return cmd.Parameters; }
        }

        public void AddParameter(string paramname, object paramvalue)
        {
            SqlParameter param = new SqlParameter(paramname, paramvalue);
            cmd.Parameters.Add(param);
        }

        public void AddParameter(IDataParameter param)
        {
            cmd.Parameters.Add(param);
        }

        public string ConnectionString
        {
            get { return strConnectionString; }
            set { strConnectionString = value; }
        }

        private void Open()
        {
            try
            {
                cmd.Connection.Open();
            }
            catch { }
        }

        private void Close()
        {
            try
            {
                cmd.Connection.Close();
            }
            catch { }
        }

        public bool HandleExpections
        {
            get { return handleErrors; }
            set { handleErrors = value; }
        }

        public string LastError
        {
            get { return strLastError; }
        }

        public void Dispose()
        {
            cmd.Dispose();
        }
        #endregion

    }
}
