﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;
using System.Configuration;
using System.Threading;
using System.Data;
using System.Xml;
using DAL;
using BAL;

namespace EAL
{
    public class EAL_Main
    {
        private BAL_Main balMain = new BAL_Main();
        public static string Authentication;
        public static string AccountFields;//
        public static string EloquaUrl = "https://secure.eloqua.com/api/bulk/2.0";
        private DataTable statustable = new DataTable();
        private DateTime sysnctime = new DateTime(); private DateTime exprtTime = new DateTime();
        private TimeSpan sysnctimediff = new TimeSpan(); private TimeSpan exprtTimediff = new TimeSpan();
        private int CustomobjectRecordCount = 0; private int InsertedRecord = 0;
        public string logname = "Merti Group";//"MEDM_Log"; 24-07-2015
        public string dbname = "[MEDM]";//  "[EloquaDataMart]"; //      //"MEDM_Log"; 24-07-2015

        //string text = "";

        public void changedatasename(string databasename)
        {
            dbname = "[" + databasename + "]";
        }

        public void changeeloquaapiname(int instanceid)
        {
            switch (instanceid)
            {
                case 7: EloquaUrl = "https://secure.p04.eloqua.com/api/bulk/2.0"; break;
                default: EloquaUrl = "https://secure.eloqua.com/api/bulk/2.0"; break;
            }
        }

        public EAL_Main(string InstanceName)
        {
            Authentication = "Basic " + GetCredential(InstanceName);//"ICIS"
        }

        //Get Credetials for authen
        public string GetCredential(string Instancename)
        {
            DataTable dt = new DataTable();
            dt = balMain.Get_Instancename(Instancename);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string Instance = Instancename;
                    string Username = dr["Username"].ToString().Trim();
                    string Password = dr["Password"].ToString().Trim();
                    string Credential = Instance + "\\" + Username + ":" + Password;
                    var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(Credential);

                    return System.Convert.ToBase64String(plainTextBytes);
                }
            }
            else
            {
                Console.WriteLine("InValiad Instance Name. Please Check Instance Name");
                return "False";
            }
            return "False";
        }

        public void ExportFromEloqua(string TableName, int InstanceId, string InstanceName, int TableId)
        {
            // $$$$$s xml
            DataTable FinalXMLTable = new DataTable();

            FinalXMLTable.Columns.Add("ContactID", typeof(string)); // this is temp
            FinalXMLTable.Columns.Add("InstanceID", typeof(string));
            FinalXMLTable.Columns.Add("xmlcolumn", typeof(string));
            // $$$$$e
            int newinstancesplit_month = 0;
            DataTable dt = new DataTable();
            /************** Get Field Count from Eloqua ********************************/
            int RecordCount = 0, RecordLoopCount = 0, RecordLimit = 1000;
            int FieldLimit = 99;
            //int FieldCount = GetFieldCount(TableName) + 1; //Find Total record count contact(244) from server
            int FieldCount = GetFieldCount(TableName, InstanceId) + 1;// from local db 03 aug 2017
            int FieldLoopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(FieldCount) / FieldLimit)); // Loop iteration
            DataTable dtExportUrl = new DataTable();
            dtExportUrl.Columns.Add("ExportUrl");
            dtExportUrl.Columns.Add("SyncUrl");
            dtExportUrl.Columns.Add("CreatedDate");
            //dtExportUrl = balMain.Get_ExportUrl(TableName);
            //417666 & 417667 & &offset=255200&limit=100
            // dtExportUrl.Rows.Add("", "/syncs/417666", DateTime.Now.ToString("yyyyMMdd")); //351644
            //dtExportUrl.Rows.Add("", "/syncs/417667", DateTime.Now.ToString("yyyyMMdd")); //351644
            //dtExportUrl.Rows.Add("", "/syncs/127021", DateTime.Now.ToString("yyyyMMdd")); //351644
            string ExportUrls = ""; string SyncUrl;
        Try_Splitmonth:
            try
            {
                int ErrorlogID = 0;

                for (int i = 1; i <= FieldLoopCount; i++)
                {
                    ExportUrls = Create_Export(TableName, i, Authentication, InstanceId, ErrorlogID, newinstancesplit_month);
                    SyncUrl = Sync_Export(ExportUrls, Authentication);
                    dtExportUrl.Rows.Add(ExportUrls, SyncUrl, DateTime.Now.ToString("yyyyMMdd"));
                }
            }
            catch (Exception e)
            {
                //string url = ExportUrls = "";
                balMain.insertTransationRecord(TableName, InstanceId, "Sync Error_ sync", ExportUrls, e.ToString());
            }
            balMain.InsertObjectHistory(InstanceId, TableId, "SyncStart");  //Insert Sync Start
            if (TableName == "contacts")   //Contact Table
            {
                CreateContact_TempTable_and_Procedure(InstanceId, InstanceName);  // This is create Temp Table and Procedure and Type table
                /// $$$$$s for deleting the contacts based on ids
                DAL_Main insertcontact1 = new DAL_Main("EloquaConfigCon"); //changed
                insertcontact1.ExecuteNonQuery("DeleteContactsBasedOnIds");
                /// $$$$$e

                string[] SyncUrl1 = new string[dtExportUrl.Rows.Count];
                int innrloop = 0; int offset = 0;
                if (dtExportUrl.Rows.Count > 0)
                {
                    bool SyncFlag = true;
                    foreach (DataRow dr in dtExportUrl.Rows)
                    {
                    Recheck:

                        string Status = Check_SyncStatus(dr["SyncUrl"].ToString(), Authentication);
                        if (Status == "error" || Status == "Error")
                        {
                            string urls = dr["SyncUrl"].ToString();
                            balMain.insertTransationRecord(TableName, InstanceId, "Object Error", urls, "Sync Errorss");
                            break;
                        }
                        else if (Status != "success")
                        {
                            SyncFlag = false;
                            Thread.Sleep(6000);
                            goto Recheck;
                        }
                        else { SyncUrl1[innrloop] = dr["SyncUrl"].ToString(); innrloop += 1; SyncFlag = true; }
                    }
                    balMain.InsertObjectHistory(InstanceId, TableId, "SyncEnd");  //Sync End

                    if (SyncFlag)
                    {
                        string syncurls = "";

                        int totalinsert = 0;
                        try
                        {
                            //foreach (DataRow dr in dtExportUrl.Rows)
                            //{
                            string response = Get_ExportData(dtExportUrl.Rows[0][1].ToString(), 0, 1, Authentication, TableName);
                            var result = JsonConvert.DeserializeObject<dynamic>(response);
                            RecordCount = Convert.ToInt32(result.totalResults.Value);

                            RecordLimit = balMain.GetRecordLimit(TableName);
                            if (RecordCount > RecordLimit) RecordLoopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(RecordCount) / RecordLimit));
                            else if (RecordCount == 0)
                            {
                                string url = syncurls + "&offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                balMain.InsertObjectHistory(InstanceId, TableId, "Exportstart");  //Export Start
                                balMain.insertTransationRecord(TableName, InstanceId, "Transation", url, "Inserted = 0");
                                balMain.insertrecordcount(TableName, RecordCount, 0, InstanceId);
                                goto End;
                            }
                            else RecordLoopCount = 1;
                            balMain.InsertObjectHistory(InstanceId, TableId, "Exportstart");  //Export Start
                            totalinsert = 0;
                            //offset = 356900;
                            for (int i = 1; i <= RecordLoopCount; i++)
                            {
                                syncurls = "";
                                DataSet dsExport = new DataSet();
                                if (i == 1) { offset = 0; }
                                else { offset = offset + RecordLimit; }
                                string[] datasettablename = { "contacts", "contacts1", "contacts2" };
                                int s = 0;

                                for (int mm = 0; mm < dtExportUrl.Rows.Count; mm++)
                                {
                                    syncurls += SyncUrl1[mm].Split('/')[2].ToString() + " & ";

                                    //by madurai offset = RecordCount - 1000;
                                    //string EloquaData = Get_ExportData(dr["SyncUrl"].ToString(), offset, RecordLimit, Authentication, TableName);
                                    string EloquaData = Get_ExportData(SyncUrl1[mm].ToString(), offset, RecordLimit, Authentication, TableName);
                                    List<string> res = EloquaData.Split(',').ToList();
                                    int totalResults = Convert.ToInt32(res[0].Split(':')[1].ToString());//{"totalResults":0

                                    if (totalResults == 0)
                                    {
                                        string urls = syncurls + "offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                        balMain.insertTransationRecord(TableName, InstanceId, "Transation", urls, "Inserted = 0");
                                    }
                                    else
                                    {
                                        if (EloquaData == null || EloquaData == "null" || EloquaData == "NULL" || EloquaData == "")
                                        {
                                            if (s <= 2)
                                            {
                                                //var synurl = SyncUrl1[mm].ToString(); var offfset = offset; var recordlimt = RecordLimit; var authn = Authentication; var tablenname = TableName;
                                                if (s == 2)
                                                {
                                                    string urls = syncurls + "offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                                    balMain.insertTransationRecord(TableName, InstanceId, "Error", urls, "Null Data");
                                                }
                                                mm -= 1;
                                            }

                                            s += 1;
                                        }
                                        else
                                        {
                                            s = 0;
                                            dsExport.Tables.Add(JsonConvert.DeserializeObject<DataTable>(JsonConvert.DeserializeObject<dynamic>(EloquaData).items.ToString()));
                                            dsExport.Tables[mm].TableName = datasettablename[mm]; //dsExport.Tables[cnt - 1].TableName = "Subscribe";//dr["TableName"].ToString();
                                            var rowcount = dsExport.Tables[mm].Rows.Count;
                                            var columncount = dsExport.Tables[mm].Columns.Count;

                                            for (int r = 0; r < rowcount; r++)
                                            {
                                                for (int c = 0; c < columncount; c++)
                                                {
                                                    if (dsExport.Tables[mm].Rows[r][c].ToString() == "") { dsExport.Tables[mm].Rows[r][c] = null; }
                                                }
                                            }
                                        }
                                    }
                                }

                                #region $$$$$s new concept of xml comehere    16-01-2017

                                // // $$$$$s new concept of xml comehere    16-01-2017

                                // DataTable copycontact1;
                                // copycontact1 = dsExport.Tables[0].Copy();

                                // DataTable sampletable1 = dsExport.Tables[0].Copy();

                                // DataSet XMLlist = balMain.GetXMLList(Convert.ToString(InstanceId), TableName);

                                // int table1count = dsExport.Tables[0].Columns.Count;

                                // if (XMLlist.Tables[0].Rows.Count > 0)
                                // {
                                //     for (int v = 0; v < table1count; v++)
                                //     {
                                //         bool contains = XMLlist.Tables[0].AsEnumerable().Any(row => sampletable1.Columns[v].ColumnName == row.Field<String>("FieldName"));

                                //         if (contains)
                                //         {
                                //             if (sampletable1.Columns[v].ColumnName != "ContactId")
                                //             {
                                //                 dsExport.Tables[0].Columns.Remove(sampletable1.Columns[v].ColumnName);
                                //             }
                                //         }
                                //         else
                                //         {
                                //             if (sampletable1.Columns[v].ColumnName != "ContactId")
                                //             {
                                //                 copycontact1.Columns.Remove(sampletable1.Columns[v].ColumnName);

                                //             }
                                //         }
                                //     }
                                // }

                                // DataTable copycontact2 = new DataTable();
                                // if (dsExport.Tables.Count > 1)
                                // {
                                //     copycontact2 = dsExport.Tables[1].Copy();

                                //     DataTable sampletable2 = dsExport.Tables[1].Copy();

                                //     int table2count = dsExport.Tables[1].Columns.Count;

                                //     if (XMLlist.Tables[0].Rows.Count > 0)
                                //     {
                                //         for (int v = 0; v < table2count; v++)
                                //         {
                                //             bool contains = XMLlist.Tables[0].AsEnumerable().Any(row => sampletable2.Columns[v].ColumnName == row.Field<String>("FieldName"));

                                //             if (contains)
                                //             {
                                //                 if (sampletable1.Columns[v].ColumnName != "ContactId")
                                //                 {
                                //                     dsExport.Tables[1].Columns.Remove(sampletable2.Columns[v].ColumnName);
                                //                 }
                                //             }
                                //             else
                                //             {
                                //                 if (sampletable2.Columns[v].ColumnName != "ContactId")
                                //                 {
                                //                     copycontact2.Columns.Remove(sampletable2.Columns[v].ColumnName);

                                //                 }
                                //             }
                                //         }
                                //     }
                                // }

                                // DataTable copycontact3 = new DataTable();
                                // if (dsExport.Tables.Count > 2)
                                // {
                                //     copycontact3 = dsExport.Tables[2].Copy();

                                //     DataTable sampletable3 = dsExport.Tables[2].Copy();

                                //     int table3count = dsExport.Tables[2].Columns.Count;

                                //     if (XMLlist.Tables[0].Rows.Count > 0)
                                //     {
                                //         for (int v = 0; v < table3count; v++)
                                //         {
                                //             bool contains = XMLlist.Tables[0].AsEnumerable().Any(row => sampletable3.Columns[v].ColumnName == row.Field<String>("FieldName"));

                                //             if (contains)
                                //             {
                                //                 if (sampletable1.Columns[v].ColumnName != "ContactId")
                                //                 {
                                //                     dsExport.Tables[2].Columns.Remove(sampletable3.Columns[v].ColumnName);
                                //                 }
                                //             }
                                //             else
                                //             {
                                //                 if (sampletable3.Columns[v].ColumnName != "ContactId")
                                //                 {
                                //                     copycontact3.Columns.Remove(sampletable3.Columns[v].ColumnName);
                                //                 }
                                //             }
                                //         }
                                //     }
                                // }

                                // var tables = new[] { copycontact1 };

                                // if (dsExport.Tables.Count > 1)
                                // {
                                //     tables = new[] { copycontact1, copycontact2 };

                                // }
                                // if (dsExport.Tables.Count > 2)
                                // {
                                //     tables = new[] { copycontact1, copycontact2, copycontact3 };
                                // }

                                // FinalXMLTable = MergeAll(tables, "ContactID");

                                // string[] columnNames = FinalXMLTable.Columns.Cast<DataColumn>()
                                //.Select(x => x.ColumnName)
                                //.ToArray();

                                // var list = new List<string>(columnNames);
                                // list.Remove("ContactID");
                                // list.Remove("InstanceID");
                                // columnNames = list.ToArray();

                                // for (int z = 0; z < FinalXMLTable.Rows.Count; z++)
                                // {
                                //     string tablevalue = "";

                                //     foreach (string str in columnNames)
                                //     {
                                //         if (!string.IsNullOrEmpty(FinalXMLTable.Rows[z][str].ToString()) && !string.IsNullOrWhiteSpace(FinalXMLTable.Rows[z][str].ToString()))
                                //         {
                                //             if (str != "ContactId")
                                //             {
                                //                 tablevalue += "<" + str + ">" + FinalXMLTable.Rows[z][str].ToString().Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;") + "</" + str + ">";
                                //             }
                                //         }
                                //     }

                                //     FinalXMLTable.Rows[z]["xmlcolumn"] = tablevalue;
                                //     FinalXMLTable.Rows[z]["InstanceID"] = InstanceId;
                                //     tablevalue = "";
                                // }

                                // // this for removing other columns from finalXMLTable

                                // DataTable Samplefinaldatatablexml = FinalXMLTable.Copy();

                                // for (int v = 0; v < Samplefinaldatatablexml.Columns.Count; v++)
                                // {
                                //     if (Samplefinaldatatablexml.Columns[v].ColumnName != "ContactId" && Samplefinaldatatablexml.Columns[v].ColumnName != "InstanceID" && Samplefinaldatatablexml.Columns[v].ColumnName != "xmlcolumn")
                                //     {
                                //         FinalXMLTable.Columns.Remove(Samplefinaldatatablexml.Columns[v].ColumnName);
                                //     }
                                // }

                                // //$$$$$e xml 16-01-2017

                                #endregion $$$$$s new concept of xml comehere    16-01-2017

                                string resultval = balMain.Insert_Update_Contacts(dsExport, InstanceId, FinalXMLTable);
                                if (resultval.ToLower().Contains(logname.ToLower())) //Maduraiveeran
                                {
                                    //balMain.SendEmail();
                                    Environment.Exit(0);
                                    //High Priority Mail Send *******************//
                                }
                                else
                                {
                                    int inserted = 0;
                                    if (resultval == "") { totalinsert += 0; }
                                    else
                                    {
                                        inserted = Convert.ToInt32(resultval);
                                        totalinsert += Convert.ToInt32(resultval);
                                    }
                                    string url = syncurls + "&offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                    balMain.insertTransationRecord(TableName, InstanceId, "Transation", url, "Inserted = " + inserted + " & from Eloqua : " + dsExport.Tables[0].Rows.Count);
                                    dsExport.Dispose();
                                }
                            }
                            balMain.insertrecordcount(TableName, RecordCount, totalinsert, InstanceId); //To insert Total records for eloqua and table

                        End:
                            totalinsert = 0;
                            //}
                        }
                        catch (Exception e)
                        {
                            var synurl = SyncUrl1[0].Split('/')[2].ToString() + " & " + SyncUrl1[1].Split('/')[2].ToString() + " & " + SyncUrl1[2].Split('/')[2].ToString() + " & ";
                            string url = synurl + "&offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString(); ;
                            balMain.insertTransationRecord(TableName, InstanceId, "Error", url, e.ToString());
                        }
                    }
                }

                ///// $$$$$s to delete contacts in temp teble
                DAL_Main insertcontact = new DAL_Main("EloquaConfigCon"); //changed
                insertcontact.ExecuteNonQuery("USP_DELETE_CONTACTS_" + InstanceName);// this is delete from deleting contact ids
                ///// $$$$$e

                balMain.insertcontacttemp_To_ContactTable(InstanceId, InstanceName);

                balMain.InsertObjectHistory(InstanceId, TableId, "ExpEnd");  //Export End Record Insert End
                // $$$$$s xml for deleting records in xml table 16-01-2017
                // balMain.DeleteInXMLContactTempTable();
                // $$$$$e xml  16-01-2017
            }
            else
            {
                int totalinsert = 0;
                if (dtExportUrl.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtExportUrl.Rows)
                    {
                    Recheck:
                        bool SyncFlag = true;
                        string Status = Check_SyncStatus(dr["SyncUrl"].ToString(), Authentication);
                        if (Status == "error" || Status == "Error")
                        {
                            string urls = dr["SyncUrl"].ToString();
                            balMain.insertTransationRecord(TableName, InstanceId, "Object Error", urls, "Sync Error");
                            break;
                        }
                        else if (Status != "success")
                        {
                            SyncFlag = false;
                            Thread.Sleep(6000);
                            goto Recheck;
                        }

                        balMain.InsertObjectHistory(InstanceId, TableId, "SyncEnd");  //Sync End
                        if (SyncFlag)
                        {
                            string response = Get_ExportData(dtExportUrl.Rows[0][1].ToString(), 0, 1, Authentication, TableName);
                            var result = JsonConvert.DeserializeObject<dynamic>(response);
                            RecordCount = Convert.ToInt32(result.totalResults.Value);

                            if (RecordCount > 4500000) { newinstancesplit_month = 1; goto Try_Splitmonth; } //This for  new instance Total record more than 45,00,000
                            RecordLimit = balMain.GetRecordLimit(TableName);
                            if (RecordCount > RecordLimit) RecordLoopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(RecordCount) / RecordLimit));
                            else if (RecordCount == 0)
                            {
                                string url = dtExportUrl.Rows[0][1].ToString() + "/data?offset=" + 0 + "&limit=" + RecordLimit.ToString();
                                balMain.InsertObjectHistory(InstanceId, TableId, "Exportstart");  //Export Start
                                balMain.insertTransationRecord(TableName, InstanceId, "Transation", url, "Inserted = 0");
                                balMain.insertrecordcount(TableName, RecordCount, 0, InstanceId);
                                break;
                            }
                            else RecordLoopCount = 1;
                            //RecordLoopCount = 1;
                            balMain.InsertObjectHistory(InstanceId, TableId, "Exportstart");  //Export Start
                            int offset = 0; totalinsert = 0;
                            for (int i = 1; i <= RecordLoopCount; i++)
                            {
                                if (i == 1) { offset = 0; }
                                else { offset = offset + RecordLimit; }
                                string[] datasettablename = { "contacts", "contacts1", "contacts2" };

                                DataSet dsExport = new DataSet();
                                int s = 0;
                                for (int mm = 0; mm < dtExportUrl.Rows.Count; mm++)
                                {
                                    try
                                    {
                                        //by madurai offset = RecordCount - 1000;
                                        string EloquaData = Get_ExportData(dr["SyncUrl"].ToString(), offset, RecordLimit, Authentication, TableName);
                                        List<string> res = EloquaData.Split(',').ToList();
                                        int totalResults = Convert.ToInt32(res[0].Split(':')[1].ToString());//{"totalResults":0
                                        if (totalResults == 0)
                                        {
                                            string urlval = dr["SyncUrl"].ToString() + "/data?offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                            balMain.insertTransationRecord(TableName, InstanceId, "Transation", urlval, "Inserted = 0");
                                        }
                                        else
                                        {
                                            if (EloquaData == null || EloquaData == "null" || EloquaData == "NULL" || EloquaData == "")
                                            {
                                                if (s <= 2)
                                                {
                                                    //var synurl = SyncUrl1[mm].ToString(); var offfset = offset; var recordlimt = RecordLimit; var authn = Authentication; var tablenname = TableName;
                                                    if (s == 2)
                                                    {
                                                        string urlval = dr["SyncUrl"].ToString() + "/data?offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                                        balMain.insertTransationRecord(TableName, InstanceId, "Error", urlval, "Null Data");
                                                    }
                                                    mm -= 1;
                                                }
                                                s += 1;
                                            }
                                            else
                                            {
                                                s = 0;
                                                dsExport.Tables.Add(JsonConvert.DeserializeObject<DataTable>(JsonConvert.DeserializeObject<dynamic>(EloquaData).items.ToString()));

                                                if (TableName != "contacts")
                                                    dsExport.Tables[mm].TableName = datasettablename[mm]; //dsExport.Tables[cnt - 1].TableName = "Subscribe";//dr["TableName"].ToString();

                                                var rowcount = dsExport.Tables[0].Rows.Count;
                                                var columncount = dsExport.Tables[0].Columns.Count;

                                                for (int r = 0; r < rowcount; r++)
                                                {
                                                    for (int c = 0; c < columncount; c++)
                                                    {
                                                        if (dsExport.Tables[0].Rows[r][c].ToString() == "") { dsExport.Tables[0].Rows[r][c] = null; }
                                                    }
                                                }
                                                string resultval = "";
                                                if (TableName == "accounts")
                                                    resultval = balMain.Insert_Update_Accounts(dsExport.Tables[0], InstanceId);
                                                else resultval = balMain.Insert_Activities(dsExport.Tables[0], InstanceId, TableName);

                                                if (resultval.ToLower().Contains(logname.ToLower())) //Maduraiveeran
                                                {
                                                    //balMain.SendEmail();
                                                    Environment.Exit(0);
                                                    //High Priority Mail Send *******************//
                                                }
                                                else
                                                {
                                                    int inserted = 0;
                                                    if (resultval == "") { totalinsert += 0; }
                                                    else
                                                    {
                                                        inserted = Convert.ToInt32(resultval);
                                                        totalinsert += Convert.ToInt32(resultval);
                                                    }
                                                    string url = dtExportUrl.Rows[0][1].ToString() + "/data?offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                                    balMain.insertTransationRecord(TableName, InstanceId, "Transation", url, "Inserted = " + inserted + " & from Eloqua : " + dsExport.Tables[0].Rows.Count);

                                                    dsExport.Dispose();
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        string url = dtExportUrl.Rows[0][1].ToString() + "/data?offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                        balMain.insertTransationRecord(TableName, InstanceId, "Error", url, e.ToString());
                                    }
                                }
                            }

                            balMain.insertrecordcount(TableName, RecordCount, totalinsert, InstanceId); //To insert Total records
                        }
                    }
                }
                balMain.InsertObjectHistory(InstanceId, TableId, "ExpEnd");  //Export End Record Insert End
            }//Else Part End
        }

        // $$$$$s xml

        public static DataTable MergeAll(IList<DataTable> tables, String primaryKeyColumn)
        {
            if (!tables.Any())
                throw new ArgumentException("Tables must not be empty", "tables");
            if (primaryKeyColumn != null)
                foreach (DataTable t in tables)
                    if (!t.Columns.Contains(primaryKeyColumn))
                        throw new ArgumentException("All tables must have the specified primarykey column " + primaryKeyColumn, "primaryKeyColumn");

            if (tables.Count == 1)
                return tables[0];

            DataTable table = new DataTable("TblUnion");
            table.BeginLoadData(); // Turns off notifications, index maintenance, and constraints while loading data
            foreach (DataTable t in tables)
            {
                table.Merge(t); // same as table.Merge(t, false, MissingSchemaAction.Add);
            }
            table.EndLoadData();

            if (primaryKeyColumn != null)
            {
                // since we might have no real primary keys defined, the rows now might have repeating fields
                // so now we're going to "join" these rows ...
                var pkGroups = table.AsEnumerable()
                    .GroupBy(r => r[primaryKeyColumn]);
                var dupGroups = pkGroups.Where(g => g.Count() > 1);
                foreach (var grpDup in dupGroups)
                {
                    // use first row and modify it
                    DataRow firstRow = grpDup.First();
                    foreach (DataColumn c in table.Columns)
                    {
                        if (firstRow.IsNull(c))
                        {
                            DataRow firstNotNullRow = grpDup.Skip(1).FirstOrDefault(r => !r.IsNull(c));
                            if (firstNotNullRow != null)
                                firstRow[c] = firstNotNullRow[c];
                        }
                    }
                    // remove all but first row
                    var rowsToRemove = grpDup.Skip(1);
                    foreach (DataRow rowToRemove in rowsToRemove)
                        table.Rows.Remove(rowToRemove);
                }
            }

            return table;
        }

        public static void apisecurity()
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        }

        // $$$$$e xml

        public static int GetFieldCount(string TableName, int InstanceId)
        {
            BAL_Main balMain = new BAL_Main();
            if (TableName == "contacts") //this is add in 03 aug 2017 for take column coumn count in local db
            {
                return balMain.TableColumncount(TableName, InstanceId);
            }
            else if (TableName == "accounts" || TableName == "contacts")
            {
                Console.WriteLine("Getting " + TableName + " Field Count .....");
                var client = new RestClient(EloquaUrl);
                var request = new RestRequest("/" + TableName + "/fields/", Method.GET);
                request.AddParameter("Authorization", Authentication, ParameterType.HttpHeader);
                apisecurity();
                var response = client.Execute(request);
                var result = JsonConvert.DeserializeObject<dynamic>(response.Content);
                return result.items.Count;
            }
            else
                return 0;
        }

        public string Create_Export(string TableName, int FieldLoopCount, string Authentication, int InstanceId, int ErrorlogID, int newinstancesplit_month)
        {
            int s = 0;
        Tryagain:
            try
            {
                string jsonString;
                var client = new RestClient(EloquaUrl);//"https://secure.eloqua.com/api/bulk/2.0"
                var currentdate = System.DateTime.Now.AddDays(-1).ToString("yyyy-M-d HH:mm:ss");  //2015-04-05 11:48:01
                Console.WriteLine("================================================================================");
                Console.WriteLine();
                Console.WriteLine("Creating Eloqua Export - Merit " + TableName + " Export");
                Console.WriteLine();
                Console.WriteLine("================================================================================");
                // var request = new RestRequest("/activities/exports", Method.POST); var request = new RestRequest("/accounts/exports", Method.POST); var request = new RestRequest("/contacts/exports", Method.POST);
                var tablename = TableName;
                if (tablename == "contacts") { tablename = "contacts"; }
                else if (tablename == "accounts") { tablename = "accounts"; }
                else { tablename = "activities"; } //InstanceId = 1;
                jsonString = balMain.GetContactfieldsjson(FieldLoopCount, TableName, InstanceId, ErrorlogID, newinstancesplit_month);
                balMain.WriteErrorlog_TextFile(jsonString);
                apisecurity();
                var request = new RestRequest("/" + tablename + "/exports", Method.POST);
                request.AddParameter("application/json; charset=utf-8", jsonString, ParameterType.RequestBody);
                request.RequestFormat = DataFormat.Json;
                request.AddParameter("Authorization", Authentication, ParameterType.HttpHeader);
                var response1 = client.Execute(request);
                var result1 = JsonConvert.DeserializeObject<dynamic>(response1.Content);
                var url1 = result1.uri;
                Console.WriteLine("================================================================================");
                Console.WriteLine("Created Merit Sample Export");
                Console.WriteLine("Export URL :" + url1.Value);
                Console.WriteLine("================================================================================");
                return url1.Value;
            }
            catch (Exception e)
            {
                if (s <= 2)
                {
                    if (s == 2)
                    {
                        balMain.insertTransationRecord(TableName, InstanceId, "Error", "Create Export", e.ToString());
                    }
                    s += 1;
                    goto Tryagain;
                }
            }
            return "";
        }

        public string Sync_Export(string Exporturl, string Authentication)
        {
            Console.WriteLine("================================================================================");
            Console.WriteLine("Synchronization Started.......");
            Console.WriteLine("================================================================================");
            string jsonString = "{\"syncedInstanceUri\":\"" + Exporturl + "\"}}";
            var client = new RestClient(EloquaUrl);//"https://secure.eloqua.com/api/bulk/2.0"
            var request = new RestRequest("/syncs", Method.POST);
            request.AddParameter("application/json; charset=utf-8", jsonString, ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;
            request.AddParameter("Authorization", Authentication, ParameterType.HttpHeader);
            var response = client.Execute(request);
            var result = JsonConvert.DeserializeObject<dynamic>(response.Content);
            var url = result.uri;
            Console.WriteLine("================================================================================");
            Console.WriteLine("Finished Synchronization");
            Console.WriteLine("url: " + url.Value);
            Console.WriteLine("================================================================================");
            Check_SyncStatus(url.Value, Authentication);
            return url.Value;
        }

        public static string Check_SyncStatus(string SyncUrl, string Authentication)
        {
            Console.WriteLine("================================================================================");
            Console.WriteLine("Checking Synchronization Status");
            var client = new RestClient(EloquaUrl);
            var request = new RestRequest(SyncUrl, Method.GET);
            request.AddParameter("Authorization", Authentication, ParameterType.HttpHeader);
            var response = client.Execute(request);
            var result = JsonConvert.DeserializeObject<dynamic>(response.Content);
            var url = result.syncedInstanceUri;
            var status = result.status;
            Console.WriteLine(status.Value);
            return status.Value;
        }

        public static string Get_ExportData(string ExportUrl, int offset, int Limit, string Authentication, string TableName)
        {
            try
            {
                Console.WriteLine("==================== " + TableName + " Export Started ====================");
                Console.WriteLine();
                Console.WriteLine("Exporting data from " + ExportUrl + "/data?offset=" + offset.ToString() + "&limit=" + Limit.ToString());
                Console.WriteLine();
                var client = new RestClient(EloquaUrl);
                var request = new RestRequest(ExportUrl + "/data?offset=" + offset.ToString() + "&limit=" + Limit.ToString(), Method.GET);
                request.AddParameter("Authorization", Authentication, ParameterType.HttpHeader);
                var response = client.Execute(request);

                return response.Content;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error(Get_ExportData) :" + e);
            }
            return "";
        }

        public void Create_CustomObjectExport(int InstanceId)
        {
            DataSet ds = new DataSet();
            statustable.Columns.Add("SyncStartTime");
            statustable.Columns.Add("SyncEndTime");
            statustable.Columns.Add("SyncTimeDiff");
            statustable.Columns.Add("ExportStartTime");
            statustable.Columns.Add("ExportEndTime");
            statustable.Columns.Add("ExportTimeDiff");
            statustable.Columns.Add("TotalRecord");

            DataTable dtCustomObject = new DataTable(); DataTable dtCustomObjectFields = new DataTable();
            ds = balMain.GetCustomObjectandFields(InstanceId);
            dtCustomObject = ds.Tables[0];
            dtCustomObjectFields = ds.Tables[1];
            CustomobjectRecordCount = 0; InsertedRecord = 0;
            int loopval = 0;
            foreach (DataRow dr in dtCustomObject.Rows)
            {
                string syncUrl = "";
                DataTable dtFields = new DataTable();
                string CustomObjectUrl = dr["uri"].ToString();
                IEnumerable<DataRow> query = from c in dtCustomObjectFields.AsEnumerable() where c.Field<Int32>("CustomObjectID") == Convert.ToInt32(dr["CustomObjectId"]) select c;
                if (query.Count() > 0)
                {
                    try
                    {
                        dtFields = query.CopyToDataTable<DataRow>();
                        Console.WriteLine("================================================================================");
                        Console.WriteLine();
                        Console.WriteLine("Creating Eloqua Export - CustomObject - " + dr["Name"].ToString());
                        Console.WriteLine();
                        Console.WriteLine("================================================================================");
                        string jsonString = balMain.GetCustomObjectfieldsJson(dr["Name"].ToString(), dtFields, InstanceId);
                        var client = new RestClient(EloquaUrl);
                        //var request = new RestRequest(CustomObjectUrl + "/exports", Method.POST);
                        var currentdate = System.DateTime.Now.AddDays(1).ToString("yyyy-M-d HH:mm:ss");  //2015-04-05 11:48:01
                        var request = new RestRequest(CustomObjectUrl + "/exports", Method.POST);
                        request.AddParameter("application/json; charset=utf-8", jsonString, ParameterType.RequestBody);
                        request.RequestFormat = DataFormat.Json;
                        request.AddParameter("Authorization", Authentication, ParameterType.HttpHeader);
                        var response = client.Execute(request);
                        var result = JsonConvert.DeserializeObject<dynamic>(response.Content);
                        var url = result.uri;
                        Console.WriteLine("================================================================================");
                        Console.WriteLine("Created Merit Sample Export");
                        Console.WriteLine("Export URL :" + url.Value);
                        Console.WriteLine("================================================================================");

                        syncUrl = Sync_Export(url.Value, Authentication);
                        get_CustomObjectExportDara(syncUrl, dr["CustomObjectId"].ToString(), Authentication, dtCustomObjectFields, InstanceId, loopval);
                    }
                    catch (Exception e)
                    {
                        string url = "";
                        balMain.insertTransationRecord("CustomObject", InstanceId, "Sync Error", "CustomObjectId " + dr["CustomObjectId"].ToString(), e.ToString());
                    }
                }
                loopval += 1;
            }
            DateTime sysnendtime = sysnctime.Add(sysnctimediff);
            DateTime exportendtime = exprtTime.Add(exprtTimediff);
            // balMain.InsertCustomObjectsyncstatus(sysnctime, sysnendtime, exprtTime, exportendtime, CustomobjectRecordCount, CustomobjectRecordCount, InstanceId);
            balMain.InsertCustomObjectsyncstatus(exprtTime, exportendtime, sysnctime, sysnendtime, CustomobjectRecordCount, CustomobjectRecordCount, InstanceId);
        }

        public void get_CustomObjectExportDara(string url, string CustomObjectId, string Authentication, DataTable dtCustomObjectFields, int InstanceId, int loopval)
        {
            DateTime t1 = DateTime.Now;// balMain.InsertObjectHistory(InstanceId, 12, "SyncStart");  //Insert Sync Start
            if (loopval == 1)
            {
                sysnctime = DateTime.Now;
                // balMain.InsertObjectHistory(InstanceId, 12, "SyncStart");
            }
        Recheck:
            Boolean SyncFlag = true;
            string Status = Check_SyncStatus(url, Authentication);
            if (Status == "error" || Status == "Error")
            {
                string urls = url;
                balMain.insertTransationRecord("CustomObjects", InstanceId, "Object Error", urls, "Sync Error");
            }
            else if (Status != "success")
            {
                SyncFlag = false;
                goto Recheck;
            }
            DateTime t2 = DateTime.Now;//balMain.InsertObjectHistory(InstanceId, 12, "SyncEnd");  //Sync End
            if (SyncFlag)
            {
                int RecordLimit = 20000, RecordLoopCount = 0;
                string response = Get_ExportData(url, 0, 1, Authentication, "CustomObjects");
                var result = JsonConvert.DeserializeObject<dynamic>(response);
                int RecordCount = Convert.ToInt32(result.totalResults.Value);
                RecordLimit = balMain.GetRecordLimit("CustomObjects");
                if (RecordCount > RecordLimit)
                    RecordLoopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(RecordCount) / RecordLimit));
                // else if (RecordCount == 0) break;
                else
                    RecordLoopCount = 1;
                if (loopval == 1)
                {
                    exprtTime = DateTime.Now;
                    //balMain.InsertObjectHistory(InstanceId, 12, "Exportstart");
                } //Export Start
                DateTime t3 = DateTime.Now;//
                int offset = 0; int s = 0;
                for (int i = 1; i <= RecordLoopCount; i++)
                {
                    try
                    {
                    Tryagain:
                        if (i == 1) { offset = 0; }
                        else { offset = offset + RecordLimit; }
                        DataTable dtData = new DataTable();
                        dtData.Columns.Add("CustomObjectId");
                        dtData.Columns.Add("FieldName");
                        dtData.Columns.Add("FieldValue");
                        dtData.Columns.Add("EntityId");
                        //offset = RecordCount - 1000;
                        dtData.Dispose();
                        DataSet dsExport = new DataSet();
                        string EloquaData = Get_ExportData(url, offset, RecordLimit, Authentication, "CustomObject");
                        List<string> reslt = EloquaData.Split(',').ToList();
                        int totalResults = Convert.ToInt32(reslt[0].Split(':')[1].ToString());//{"totalResults":0
                        if (totalResults == 0)
                        {
                            string urlval = url + "/data?offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                            balMain.insertTransationRecord("CustomObjects", InstanceId, "Transation", urlval, "Inserted = 0");
                        }
                        else
                        {
                            if (EloquaData == null || EloquaData == "null" || EloquaData == "NULL" || EloquaData == "")
                            {
                                if (s <= 2)
                                {
                                    //var synurl = SyncUrl1[mm].ToString(); var offfset = offset; var recordlimt = RecordLimit; var authn = Authentication; var tablenname = TableName;
                                    if (s == 2)
                                    {
                                        string urlval = url + "/data?offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                        balMain.insertTransationRecord("CustomObject", InstanceId, "Error", urlval, "Null Data");
                                    }
                                    s += 1;
                                    i -= 1;
                                    goto Tryagain;
                                }
                            }
                            else
                            {
                                s = 0;

                                if (JsonConvert.DeserializeObject<dynamic>(EloquaData).totalResults.ToString() != "0")
                                {
                                    dsExport.Tables.Add(JsonConvert.DeserializeObject<DataTable>(JsonConvert.DeserializeObject<dynamic>(EloquaData).items.ToString()));

                                    DataTable Entityval = new DataTable();
                                    BAL_Main res = new BAL_Main();
                                    Entityval = res.GetCustomobjectdatatopid(InstanceId);//.Executesql("select top 1 EntityId from  CustomObjectData order by Id desc", Entityval, "");
                                    int id = 0;
                                    foreach (DataRow dr in Entityval.Rows)
                                    {
                                        id = Convert.ToInt32(dr["EntityId"].ToString());
                                    }

                                    foreach (DataRow dr in dsExport.Tables[0].Rows)
                                    {
                                        id += 1;// id + 1;
                                        int columnCount = dsExport.Tables[0].Columns.Count;
                                        for (int j = 0; j < columnCount; j++)
                                        {
                                            var Query = (from c in dtCustomObjectFields.AsEnumerable() where c.Field<string>("internalName") == dsExport.Tables[0].Columns[j].ColumnName select c).FirstOrDefault();
                                            if (dr[j].ToString() != null && dr[j].ToString() != "")//this is avoid null FieldVal
                                                dtData.Rows.Add(CustomObjectId, Query.ItemArray[3].ToString(), dr[j].ToString(), id);
                                            //dtData.Rows.Add(CustomObjectId, Query.ItemArray[2].ToString(), dr[j].ToString(), id);
                                        }
                                    }
                                    BAL_Main objGeneral = new BAL_Main();
                                    int insertrecd = 0;
                                    string resultval = objGeneral.Insert_CustomObjectData(dtData, InstanceId);
                                    if (resultval.ToLower().Contains(logname.ToLower())) //Maduraiveeran
                                    {
                                        // balMain.SendEmail();
                                        Environment.Exit(0);
                                        //High Priority Mail Send *******************//
                                    }
                                    else
                                    {
                                        if (resultval == "") { insertrecd = 0; }
                                        else
                                        {
                                            insertrecd = Convert.ToInt32(resultval);
                                            //text += "URL VaLue : " + url + "\r\nCustomObjectId : " + CustomObjectId + "\r\n Eloqua Record : "+RecordCount+"\r\n  Insert Record : " +insertrecd+"\r\n----------------------------------------------\r\n";
                                            //System.IO.File.WriteAllText(@"D:\Maduraiveeran\Projects\Svn\Eloqua_DET\CustomobejctLog.txt", text);
                                        }
                                        InsertedRecord += insertrecd; //Sum Inserted Record Count
                                        dtData.Dispose();
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        string urlval = url + "/data?offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString() + "|" + CustomObjectId;
                        balMain.insertTransationRecord("CustomObject", InstanceId, "Error", urlval, e.ToString());
                    }
                }
                DateTime t4 = DateTime.Now;// balMain.InsertObjectHistory(InstanceId, 12, "ExpEnd");  //Export End Record Insert End
                sysnctimediff += t2.Subtract(t1);
                exprtTimediff += t4.Subtract(t3);
                CustomobjectRecordCount += RecordCount;

                // statustable.Rows.Add(t1.ToString("HH:mm:ss"), t2.ToString("HH:mm:ss"), sysnctimediff, t3.ToString("HH:mm:ss"), t4.ToString("HH:mm:ss"), exprtTimediff, CustomobjectRecordCount);
                Console.WriteLine("CustomObject Export Completed --> " + CustomobjectRecordCount + " Records Inserted to CustomObject Table.");
            }
        }

        public void CreateTypetableVariable(string TableName, int Instance, string InstanceName)
        {
            string InstanceId = Convert.ToString(Instance);
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            List<string> li1 = new List<string>(); List<string> li2 = new List<string>();
            ds = balMain.GetTableFiekdName(TableName, Instance);
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                { li1.Add(dr.ItemArray[0].ToString()); }
            }
            if (ds.Tables[1].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[1].Rows)
                {
                    li2.Add(dr.ItemArray[0].ToString());
                }
            } string temptable = "";
            var maxinstanceid = ds.Tables[2].Rows[0][0].ToString();
            string[] list1 = li1.ToArray(); //Currrent Instance Record
            string[] list2 = li2.ToArray(); //Old Instance Records
            //int loopcount=list1.Count()>list2.Count()?list1.Count():list2.Count();
            for (int k = 0; k < list1.Count(); k++)
            {
                int m = 0;
                for (int l = 0; l < list2.Count(); l++)
                {
                    var name2 = list2[l].Replace("_", "").Trim();//.Replace("C_FG_", "")
                    var name1 = list1[k].Replace("_", "").Trim(); //.Replace("C_CH_", "")
                    if (name1 == name2)
                    {
                        string oldFieldName = list1[k].ToString();
                        string newFieldName = list2[l].ToString();
                        if (TableName == "Contact") { TableName = "contacts"; }
                        if (TableName == "Account") { TableName = "accounts"; }
                        balMain.altercolumnname(TableName, newFieldName, oldFieldName, Convert.ToInt16(InstanceId));
                        m = 0; break;
                    }
                    else { m = 1; }
                }
                if (m == 1)
                {
                    if (TableName == "contacts" || TableName == "Contact")
                    {
                        TableName = "Contact";
                        temptable = dbname + "..Contact";
                    }
                    if (TableName == "accounts")
                    {
                        TableName = "Account";
                        temptable = dbname + "..Account";
                    }
                    balMain.CreateAlterColumn(temptable, list1[k], "", "Add");
                }
            }
            CreateTypeTable_and_Procedure_New(TableName, InstanceId, InstanceName);
        }

        public void CreateTypeTable_and_Procedure_New(string TableName, string InstanceId, string InstanceName)
        {
            string typetable1 = ""; string typetable2 = ""; string typetable3 = ""; string typetable4 = ""; string typetable5 = ""; int i = 0;

            if (TableName == "contacts" || TableName == "Contact")
            {
                string DbwithTableName = dbname + "..Contact";
                TableName = "Contact";
                string stringsp1 = "";
                string stringsp4 = "";
                string typename = "";
                string newinstance = " Declare @checknewinstance bit  set @checknewinstance=(select NewInstance from EloquaConfig..Instances where InstanceId=@InstanceId) if(@checknewinstance=0) begin ";
                newinstance += " INSERT INTO " + DbwithTableName + " ( ContactId ,";
                //string proc1 = ",@new_identity INT = NULL OUTPUT ) AS BEGIN BEGIN TRY    BEGIN TRANSACTION T1  ";
                string proc1 = ",@new_identity INT = NULL OUTPUT ) AS BEGIN BEGIN TRY  Declare @RecordCount Table (Cnt varchar(100))   BEGIN TRANSACTION T1  "; //Added 23-07-2015 for @RecordCount

                string str1 = ""; string typetablename1 = ""; string typetablename2 = ""; string typetablename3 = ""; string typetablename4 = ""; string typetablename5 = "";
                string str3 = "ContactId=S_Table.ContactId, ";
                var strupdate = "update b set ";
                DataTable dt = balMain.getTablecolumnFieldsforinstance(TableName, InstanceId);
                if (dt.Rows.Count > 0)
                {
                    typetable1 += "CREATE TYPE [dbo].[Type_Contact_First_" + InstanceName + "] AS TABLE( [ContactId] [int] NULL,";
                    typetable2 += "CREATE TYPE [dbo].[Type_Contact_Second_" + InstanceName + "] AS TABLE( [ContactId] [int] NULL,";
                    typetable3 += "CREATE TYPE [dbo].[Type_Contact_Third_" + InstanceName + "] AS TABLE( [ContactId] [int] NULL,";
                    typetable4 += "CREATE TYPE [dbo].[Type_Contact_Fourth_" + InstanceName + "] AS TABLE( [ContactId] [int] NULL,";
                    ///// $$$$$s for type table 5
                    typetable5 += "CREATE TYPE [dbo].[Type_Contact_Fifth_" + InstanceName + "] AS TABLE( [ContactId] [int] NULL,";
                    ///// $$$$$e
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (i > 0)
                        {
                            string fieldname = dr["FieldName"].ToString();
                            stringsp1 += fieldname + ",";
                            stringsp4 += "S_Table." + fieldname + " ,";
                            str3 += fieldname + " = S_Table." + fieldname + " ,";
                            if (i <= 99) { typetable1 += "[" + fieldname + "]  [nvarchar](max) NULL,"; str1 += "F." + fieldname + ","; }//strupdate += " " + fieldname + " = F." + fieldname + ",";
                            if (i > 99 && i <= 198) { typetable2 += "[" + fieldname + "]  [nvarchar](max) NULL,"; str1 += "S." + fieldname + ","; }// strupdate += " " + fieldname + " = S." + fieldname + ",";
                            if (i > 198 && i <= 297) { typetable3 += "[" + fieldname + "]  [nvarchar](max) NULL,"; str1 += "T." + fieldname + ","; }//strupdate += " " + fieldname + " = T." + fieldname + ",";
                            if (i > 297 && i <= 396) { typetable4 += "[" + fieldname + "]  [nvarchar](max) NULL,"; str1 += "Fr." + fieldname + ","; }// strupdate += " " + fieldname + " = Fr." + fieldname + ",";
                            ///// $$$$$s for more than 400
                            if (i > 396 && i <= 495) { typetable5 += "[" + fieldname + "] [nvarchar](max) NULL,"; str1 += "Fi." + fieldname + ","; }
                            ///// $$$$$e

                            if (i <= 99) { typetablename1 = "@FirstPart F  "; }
                            if (i > 99 && i <= 198) { typetablename2 = "  inner join  @SecondPart S  ON S.ContactID = F.ContactID "; }
                            if (i > 198 && i <= 297) { typetablename3 = "  inner join   @ThirdPart T  ON T.ContactID = F.ContactID  "; }
                            if (i > 297 && i <= 396) { typetablename4 = "  inner join  @Fourth Fr  ON Fr.ContactID = F.ContactID  "; }
                            ///// $$$$$s this is for fifth table name
                            if (i > 396 && i <= 495) { typetablename5 = "  inner join  @Fifth  Fi  ON Fi.ContactID = F.ContactID  "; }
                            ///// $$$$$e
                        }
                        i += 1;
                    }
                }

                if (typetable1.Count() > 100) { typetable1 = typetable1.TrimEnd(',') + ")"; balMain.creeateTypetable("Type_Contact_First_" + InstanceName + "", typetable1, "USP_Insert_Update_Contacts_" + InstanceName, "USP_Insert_Contacts_" + InstanceName + "_Temp"); typename += "@FirstPart F "; }// strupdate += " @FirstPart F";
                if (typetable2.Count() > 100) { typetable2 = typetable2.TrimEnd(',') + ")"; balMain.creeateTypetable("Type_Contact_Second_" + InstanceName + "", typetable2, "USP_Insert_Update_Contacts_" + InstanceName, "USP_Insert_Contacts_" + InstanceName + "_Temp"); typename += "INNER JOIN @SecondPart S ON F.ContactId = S.ContactId "; }//  strupdate += " INNER JOIN @SecondPart S ON F.ContactId = S.ContactId";
                if (typetable3.Count() > 100) { typetable3 = typetable3.TrimEnd(',') + ")"; balMain.creeateTypetable("Type_Contact_Third_" + InstanceName + "", typetable3, "USP_Insert_Update_Contacts_" + InstanceName, "USP_Insert_Contacts_" + InstanceName + "_Temp"); typename += "INNER JOIN @ThirdPart T ON T.ContactId = F.ContactId "; }// strupdate += " INNER JOIN @ThirdPart T ON T.ContactId = F.ContactId";
                if (typetable4.Count() > 100) { typetable4 = typetable4.TrimEnd(',') + ")"; balMain.creeateTypetable("Type_Contact_Fourth_" + InstanceName + "", typetable4, "USP_Insert_Update_Contacts_" + InstanceName, "USP_Insert_Contacts_" + InstanceName + "_Temp"); typename += "INNER JOIN @Fourth Fr ON Fr.ContactId = F.ContactId "; }//  strupdate += " INNER JOIN @Fourth Fr ON Fr.ContactId = F.ContactId ";
                ///// $$$$$s code for typetable5
                if (typetable5.Count() > 100) { typetable5 = typetable5.TrimEnd(',') + ")"; balMain.creeateTypetable("Type_Contact_Fifth_" + InstanceName + "", typetable5, "USP_Insert_Update_Contacts_" + InstanceName, "USP_Insert_Contacts_" + InstanceName + "_Temp"); typename += "INNER JOIN @Fifth Fi ON Fi.ContactId = F.ContactId "; }
                ///// $$$$$e

                string finalstr = "CREATE PROCEDURE [dbo].[USP_Insert_Update_Contacts_" + InstanceName + "](@Type varchar(100),  @InstanceId INT , @FirstPart Type_Contact_First_" + InstanceName + " READONLY";

                if (typetable2.Count() > 100) { finalstr += ", @SecondPart Type_Contact_Second_" + InstanceName + " READONLY"; }
                if (typetable3.Count() > 100) { finalstr += ",@ThirdPart Type_Contact_Third_" + InstanceName + " READONLY "; }
                if (typetable4.Count() > 100) { finalstr += ",@Fourth Type_Contact_Fourth_" + InstanceName + " READONLY"; }
                ///// $$$$$s this is for fifth table
                if (typetable5.Count() > 100) { finalstr += ",@Fifth Type_Contact_Fifth_" + InstanceName + " READONLY"; }
                ///// $$$$$e

                newinstance += stringsp1 + " InstanceId ,CreatedDate,UpdatedDate  ) select F.ContactId, " + str1 + " @InstanceId,GETDATE(),'' From " + typename + "ORDER BY F.ContactId ASC SET @new_identity = @@ROWCOUNT SELECT @new_identity END else begin ";

                proc1 += newinstance + " ;MERGE  " + DbwithTableName + " AS T_TABLE  Using ( select F.ContactId," + str1 + " @InstanceId InstanceId, GETDATE() CreatedDate,''  UpdatedDate from " + typetablename1 + " " + typetablename2 + " " + typetablename3 + " " + typetablename4 + "   ) as S_Table ON S_Table.ContactId=T_Table.ContactId  and T_Table.InstanceId=@InstanceId ";
                //SELECT F.ContactId ," + str1 + " @InstanceId ,GETDATE() ,NULL FROM ";
                proc1 += " When MATCHED then Update set  " + str3 + " InstanceId = @InstanceId, UpdatedDate = GETDATE()";
                proc1 += " When Not MATCHED Then Insert (ContactId, " + stringsp1 + " InstanceId, CreatedDate, UpdatedDate ) values (S_Table.ContactId, " + stringsp4 + " @InstanceId, GETDATE(), '') ";
                //strupdate += " InstanceId=@InstanceId,UpdatedDate=GETDATE() FROM ";

                // string trycatcherr = " SET @new_identity = @@ROWCOUNT   SELECT @new_identity  end  COMMIT TRANSACTION T1   END TRY  BEGIN CATCH IF @@TRANCOUNT > 0  ROLLBACK TRANSACTION T1  INSERT INTO EloquaConfig..TransactionLog (InstanceId,TableName,LogType,Mssg,LogDate)";
                string trycatcherr = " OUTPUT $action into @RecordCount; SET @new_identity = (select COUNT(*) from @RecordCount)   SELECT @new_identity  end  COMMIT TRANSACTION T1   END TRY  BEGIN CATCH IF @@TRANCOUNT > 0  ROLLBACK TRANSACTION T1  INSERT INTO EloquaConfig..TransactionLog (InstanceId,TableName,LogType,Mssg,LogDate)";
                //The above line changed for
                trycatcherr += " VALUES (@InstanceId,'Contact','DataBase Err',ERROR_MESSAGE(),GETDATE())   SET @new_identity = ERROR_MESSAGE()  SELECT @new_identity  END CATCH  end";
                finalstr = finalstr + proc1 + " " + trycatcherr;
                ///// $$$$$s create dynamic procedure for deleting contacts in temp table

                string deletecontactprocedure = "CREATE PROCEDURE [dbo].[USP_DELETE_CONTACTS_" + InstanceName + "] AS BEGIN ";

                deletecontactprocedure += "delete from Contact_First_" + InstanceName + " where ContactId in (select contactid from DeletedContacts   where IsDelete = 0 or IsDelete is NULL); ";

                if (i > 99 && i <= 198)
                {
                    deletecontactprocedure += "delete from Contact_Second_" + InstanceName + " where ContactId in (Select contactid from DeletedContacts   where IsDelete = 0 or IsDelete is NULL); ";
                }
                if (i > 198 && i <= 297)
                {
                    deletecontactprocedure += "delete from Contact_Third_" + InstanceName + " where ContactId in (Select contactid from DeletedContacts   where IsDelete = 0 or IsDelete is NULL); ";
                }
                if (i > 297 && i <= 396)
                {
                    deletecontactprocedure += "delete from Contact_Fourth_" + InstanceName + " where ContactId in (Select contactid from DeletedContacts   where IsDelete = 0 or IsDelete is NULL); ";
                }
                if (i > 396 && i <= 495)
                {
                    deletecontactprocedure += "delete from Contact_Fifth_" + InstanceName + " where ContactId in (Select contactid from DeletedContacts  where IsDelete = 0 or IsDelete is NULL); ";
                }
                deletecontactprocedure += " END";

                balMain.Createprocedure(deletecontactprocedure, "USP_DELETE_CONTACTS_" + InstanceName);

                ///// $$$$$e

                //this is removed because this created for contact object call
                balMain.Createprocedure(finalstr, "USP_Insert_Update_Contacts_" + InstanceName);
            }
            else if (TableName == "accounts" || TableName == "Account")
            {
                TableName = "Account";
                string DbwithTableName = dbname + "..Account";
                string newinstance = " Declare @checknewinstance bit  set @checknewinstance=(select NewInstance from EloquaConfig..Instances where InstanceId=@InstanceId) if(@checknewinstance=0) begin ";
                newinstance += " INSERT INTO " + DbwithTableName + " ( AccountId ,";
                string typetable_a1 = "CREATE TYPE [dbo].[Type_Account_" + InstanceName + "] AS TABLE([AccountId] [int] NULL,";
                //typetable2 = "CREATE TYPE [dbo].[Type_Account_First_"+InstanceName+"] AS TABLE([AccountId] [int] NULL,";
                string proc1 = "CREATE PROCEDURE [dbo].[USP_Add_Update_Account_" + InstanceName + "](@Type varchar(100), @InstanceId int, @Account Type_Account_" + InstanceName + " READONLY ,@new_identity INT = NULL OUTPUT) ";
                proc1 += " AS BEGIN BEGIN TRY    BEGIN TRANSACTION T1 ";
                //string proc2 = "CREATE PROCEDURE [dbo].[USP_Add_Update_Account_"+InstanceName+"](@InstanceId int, @Account Type_Account READONLY ) AS BEGIN INSERT INTO Account (AccountId,";
                string str1 = ""; string str2 = " select a.AccountId, "; string str3 = "AccountId=S_Table.AccountId,"; string str4 = "S_TABLE.AccountId, ";
                DataTable dt = balMain.getTablecolumnFieldsforinstance(TableName, InstanceId);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (i > 0)
                        {
                            string fieldname = dr["FieldName"].ToString();
                            if (i <= 99) { typetable_a1 += "[" + fieldname + "]  [nvarchar](max) NULL,"; str1 += fieldname + " ,"; str2 += "a." + fieldname + " ,"; str3 += fieldname + " = S_Table." + fieldname + " ,"; str4 += " S_Table." + fieldname + " ,"; }
                            //if (i > 99 && i <= 198)  { typetable2 += "[" + fieldname + "]  [varchar](1000) NULL,";  }
                        }
                        i += 1;
                    }
                }
                if (typetable_a1.Count() > 100) { typetable_a1 = typetable_a1.TrimEnd(',') + ")"; balMain.creeateTypetable("Type_Account_" + InstanceName, typetable_a1, "USP_Add_Update_Account_" + InstanceName, ""); }
                // if (typetable2.Count() > 100) {  typetable2 = typetable2.TrimEnd(',') + ")";  balMain.creeateTypetable("Type_Account_First", typetable2); }
                newinstance += str1 + " InstanceId ,CreatedDate,UpdatedDate  )  " + str2 + " @InstanceId,GETDATE(),'' From @Account a ORDER BY a.AccountId ASC SET @new_identity = @@ROWCOUNT SELECT @new_identity END else begin ";

                string finalstr = proc1 + newinstance + "  ;MERGE  " + DbwithTableName + " as  T_TABLE  Using  ( " + str2 + " @InstanceId InstanceId, GETDATE() CreatedDate, ''   UpdatedDate FROM @Account a) as S_Table On S_Table.AccountId=T_TABLE.AccountId and T_TABLE.InstanceId=@InstanceId  ";
                finalstr += " WHEN MATCHED THEN update set " + str3 + "InstanceId = @InstanceId ,UpdatedDate = GETDATE() ";
                finalstr += " WHEN NOT MATCHED Then insert (AccountId," + str1 + " InstanceId, CreatedDate, UpdatedDate) Values(" + str4 + " @InstanceId,GETDATE(),''); SET @new_identity = @@ROWCOUNT  SELECT @new_identity   ";
                string trycatcherr = " end COMMIT TRANSACTION T1   END TRY  BEGIN CATCH IF @@TRANCOUNT > 0  ROLLBACK TRANSACTION T1  INSERT INTO EloquaConfig..TransactionLog (InstanceId,TableName,LogType,Mssg,LogDate)";
                trycatcherr += " VALUES(@InstanceId,'Account','DataBase Err',ERROR_MESSAGE(),GETDATE())   SET @new_identity = ERROR_MESSAGE()  SELECT @new_identity  END CATCH end";
                finalstr += trycatcherr;

                balMain.Createprocedure(finalstr, "USP_Add_Update_Account_" + InstanceName);
            }
        }

        #region Old CreateTypeTable_and_Procedure

        public void CreateTypeTable_and_Procedure(string TableName, string InstanceId, string InstanceName)
        {
            //string typetable1 = ""; string typetable2 = ""; string typetable3 = ""; string typetable4 = ""; int i = 0;

            //if (TableName == "contacts" || TableName == "Contact")
            //{
            //    string DbwithTableName = " "+dbname+"..Contact  ";

            //    TableName = "Contact";
            //    string stringsp1 = "AS BEGIN BEGIN TRY    BEGIN TRANSACTION T1    if(@Type='Update') begin INSERT  INTO " + DbwithTableName + " (ContactId,";
            //    string str1 = "";
            //    var strupdate = "update b set ";
            //    DataTable dt = balMain.getTablecolumnFieldsforinstance(TableName, InstanceId);
            //    if (dt.Rows.Count > 0)
            //    {
            //        typetable1 += "CREATE TYPE [dbo].[Type_Contact_First_" + InstanceName + "] AS TABLE( [ContactId] [int] NULL,";
            //        typetable2 += "CREATE TYPE [dbo].[Type_Contact_Second_" + InstanceName + "] AS TABLE( [ContactId] [int] NULL,";
            //        typetable3 += "CREATE TYPE [dbo].[Type_Contact_Third_" + InstanceName + "] AS TABLE( [ContactId] [int] NULL,";
            //        typetable4 += "CREATE TYPE [dbo].[Type_Contact_Fourth_" + InstanceName + "] AS TABLE( [ContactId] [int] NULL,";
            //        foreach (DataRow dr in dt.Rows)
            //        {
            //            if (i > 0)
            //            {
            //                string fieldname = dr["FieldName"].ToString();
            //                stringsp1 += fieldname + ",";
            //                if (i <= 99) { typetable1 += "[" + fieldname + "]  [nvarchar](max) NULL,"; str1 += "F." + fieldname + ","; strupdate += " " + fieldname + " = F." + fieldname + ","; }
            //                if (i > 99 && i <= 198) { typetable2 += "[" + fieldname + "]  [nvarchar](max) NULL,"; str1 += "S." + fieldname + ","; strupdate += " " + fieldname + " = S." + fieldname + ","; }
            //                if (i > 198 && i <= 297) { typetable3 += "[" + fieldname + "]  [nvarchar](max) NULL,"; str1 += "T." + fieldname + ","; strupdate += " " + fieldname + " = T." + fieldname + ","; }
            //                if (i > 297 && i <= 396) { typetable4 += "[" + fieldname + "]  [nvarchar](max) NULL,"; str1 += "Fr." + fieldname + ","; strupdate += " " + fieldname + " = Fr." + fieldname + ","; }
            //            }
            //            i += 1;
            //        }
            //    }
            //    stringsp1 += " InstanceId, CreatedDate, UpdatedDate) SELECT F.ContactId ," + str1 + " @InstanceId ,GETDATE() ,NULL FROM ";
            //    strupdate += " InstanceId=@InstanceId,UpdatedDate=GETDATE() FROM ";
            //    if (typetable1.Count() > 100) { typetable1 = typetable1.TrimEnd(',') + ")"; balMain.creeateTypetable("Type_Contact_First_" + InstanceName + "", typetable1, "USP_Insert_Update_Contacts_" + InstanceName); stringsp1 += "@FirstPart F "; strupdate += " @FirstPart F"; }
            //    if (typetable2.Count() > 100) { typetable2 = typetable2.TrimEnd(',') + ")"; balMain.creeateTypetable("Type_Contact_Second_" + InstanceName + "", typetable2, "USP_Insert_Update_Contacts_" + InstanceName); stringsp1 += "INNER JOIN @SecondPart S ON F.ContactId = S.ContactId "; strupdate += " INNER JOIN @SecondPart S ON F.ContactId = S.ContactId"; }
            //    if (typetable3.Count() > 100) { typetable3 = typetable3.TrimEnd(',') + ")"; balMain.creeateTypetable("Type_Contact_Third_" + InstanceName + "", typetable3, "USP_Insert_Update_Contacts_" + InstanceName); stringsp1 += "INNER JOIN @ThirdPart T ON T.ContactId = F.ContactId "; strupdate += " INNER JOIN @ThirdPart T ON T.ContactId = F.ContactId"; }
            //    if (typetable4.Count() > 100) { typetable4 = typetable4.TrimEnd(',') + ")"; balMain.creeateTypetable("Type_Contact_Fourth_" + InstanceName + "", typetable4, "USP_Insert_Update_Contacts_" + InstanceName); stringsp1 += "INNER JOIN @Fourth Fr ON Fr.ContactId = F.ContactId "; strupdate += " INNER JOIN @Fourth Fr ON Fr.ContactId = F.ContactId "; }

            //    string elsepart = stringsp1.Replace("AS BEGIN BEGIN TRY    BEGIN TRANSACTION T1    if(@Type='Update') begin ", " ") + " ORDER BY F.ContactId ASC  SET @new_identity = @@ROWCOUNT  SELECT @new_identity end";
            //    stringsp1 += "left outer join  " + DbwithTableName + " b on F.ContactId=b.ContactId  and b.InstanceId=@InstanceId where b.ContactId is null ORDER BY F.ContactId ASC  SET @new_identity = @@ROWCOUNT  SELECT @new_identity ";
            //    strupdate += " inner join  " + DbwithTableName + " b on F.ContactId=b.ContactId and  b.InstanceId=@InstanceId End ";
            //    strupdate += " else begin " + elsepart; //Else part Added

            //    string finalstr = "CREATE PROCEDURE [dbo].[USP_Insert_Update_Contacts_" + InstanceName + "](@Type varchar(100),  @InstanceId INT , @FirstPart Type_Contact_First_" + InstanceName + " READONLY , @SecondPart Type_Contact_Second_" + InstanceName + " READONLY";
            //    if (typetable3.Count() > 100) { finalstr += ",@ThirdPart Type_Contact_Third_" + InstanceName + " READONLY "; }
            //    if (typetable4.Count() > 100) { finalstr += ",@Fourth Type_Contact_Fourth_" + InstanceName + " READONLY"; }

            //    string trycatcherr = "  COMMIT TRANSACTION T1   END TRY  BEGIN CATCH IF @@TRANCOUNT > 0  ROLLBACK TRANSACTION T1  INSERT INTO EloquaConfig..TransactionLog (InstanceId,TableName,LogType,Mssg,LogDate)";
            //    trycatcherr += "VALUES(@InstanceId,'Contact','DataBase Err',ERROR_MESSAGE(),GETDATE())   SET @new_identity = ERROR_MESSAGE()  SELECT @new_identity  END CATCH  end";
            //    finalstr = finalstr + ", @new_identity INT = NULL OUTPUT ) " + stringsp1 + " " + strupdate + trycatcherr;
            //    balMain.Createprocedure(finalstr, "USP_Insert_Update_Contacts_" + InstanceName);

            //}
            //else if (TableName == "accounts" || TableName == "Account")
            //{
            //    TableName = "Account";
            //    string DbwithTableName =" "+dbname+"..Account  ";/
            //    string typetable_a1 = "CREATE TYPE [dbo].[Type_Account_" + InstanceName + "] AS TABLE([AccountId] [int] NULL,";
            //    //typetable2 = "CREATE TYPE [dbo].[Type_Account_First_"+InstanceName+"] AS TABLE([AccountId] [int] NULL,";
            //    string proc1 = "CREATE PROCEDURE [dbo].[USP_Add_Update_Account_" + InstanceName + "](@Type varchar(100), @InstanceId int, @Account Type_Account_" + InstanceName + " READONLY ,@new_identity INT = NULL OUTPUT) AS BEGIN BEGIN TRY    BEGIN TRANSACTION T1  if(@Type='Update') begin INSERT INTO  " + DbwithTableName + " (AccountId,";
            //    //string proc2 = "CREATE PROCEDURE [dbo].[USP_Add_Update_Account_"+InstanceName+"](@InstanceId int, @Account Type_Account READONLY ) AS BEGIN INSERT INTO Account (AccountId,";
            //    string str1 = ""; string str2 = "select a.AccountId, "; string str3 = "update b set AccountId=a.AccountId,";
            //    DataTable dt = balMain.getTablecolumnFieldsforinstance(TableName, InstanceId);
            //    if (dt.Rows.Count > 0)
            //    {
            //        foreach (DataRow dr in dt.Rows)
            //        {
            //            if (i > 0)
            //            {
            //                string fieldname = dr["FieldName"].ToString();

            //                if (i <= 99) { typetable_a1 += "[" + fieldname + "]  [nvarchar](max) NULL,"; str1 += fieldname + " ,"; str2 += "a." + fieldname + " ,"; str3 += fieldname + " = a." + fieldname + " ,"; }
            //                //if (i > 99 && i <= 198)  { typetable2 += "[" + fieldname + "]  [varchar](1000) NULL,";  }
            //            }
            //            i += 1;
            //        }
            //    }
            //    if (typetable_a1.Count() > 100) { typetable_a1 = typetable_a1.TrimEnd(',') + ")"; balMain.creeateTypetable("Type_Account_" + InstanceName, typetable_a1, "USP_Add_Update_Account_" + InstanceName); }
            //    // if (typetable2.Count() > 100) {  typetable2 = typetable2.TrimEnd(',') + ")";  balMain.creeateTypetable("Type_Account_First", typetable2); }
            //    string finalstr = proc1 + str1 + " InstanceId, CreatedDate, UpdatedDate)  " + str2 + "@InstanceId,GETDATE(),NULL From @Account a left outer join  " + DbwithTableName + " b on a.AccountId=b.AccountId and b.InstanceId=@InstanceId where b.AccountId is null order by a.AccountId Asc SET @new_identity = @@ROWCOUNT  SELECT @new_identity   ";
            //    finalstr += str3 + " InstanceId=@InstanceId, UpdatedDate=GETDATE() from @Account a inner join " + DbwithTableName + " b on a.AccountId=b.AccountId and  b.InstanceId=@InstanceId END ";
            //    finalstr += " else begin INSERT INTO  " + DbwithTableName + " (AccountId," + str1 + " InstanceId, CreatedDate, UpdatedDate)  " + str2 + " @InstanceId,GETDATE(),NULL From @Account a order by a.AccountId Asc SET @new_identity = @@ROWCOUNT  SELECT @new_identity   end "; //Else part
            //    string trycatcherr = "  COMMIT TRANSACTION T1   END TRY  BEGIN CATCH IF @@TRANCOUNT > 0  ROLLBACK TRANSACTION T1  INSERT INTO EloquaConfig..TransactionLog (InstanceId,TableName,LogType,Mssg,LogDate)";
            //    trycatcherr += "VALUES(@InstanceId,'Account','DataBase Err',ERROR_MESSAGE(),GETDATE())   SET @new_identity = ERROR_MESSAGE()  SELECT @new_identity  END CATCH end";
            //    finalstr += trycatcherr;

            //    balMain.Createprocedure(finalstr, "USP_Add_Update_Account_" + InstanceName);

            //}
        }

        #endregion Old CreateTypeTable_and_Procedure

        public void GetFields(string TableName, int InstanceId)
        {
            Console.WriteLine("================================================================================");
            Console.WriteLine("Getting " + TableName + " Fields .....");
            // TryAgain:
            try
            {
                //mm  string Authentication = "Basic " + GetCredential();
                var client = new RestClient(EloquaUrl);

                var request = new RestRequest("/" + TableName + "/fields/", Method.GET);
                request.AddParameter("Authorization", Authentication, ParameterType.HttpHeader);
                var response = client.Execute(request);
                var result = JsonConvert.DeserializeObject<dynamic>(response.Content);
                //Console.WriteLine(result.ToString());
                DataSet ds = new DataSet();

                //var request = new RestRequest(ExportUrl + "/data?offset=" + offset.ToString() + "&limit=" + Limit.ToString(), Method.GET);
                //request.AddParameter("Authorization", Authentication, ParameterType.HttpHeader);
                //var response = client.Execute(request);
                // return response.Content;
                ds.Tables.Add(JsonConvert.DeserializeObject<DataTable>(JsonConvert.DeserializeObject<dynamic>(response.Content).items.ToString()));
                DataSet dataset = new DataSet();
                DataTable dt = new DataTable();
                dt.Columns.Add("FieldName");
                dt.Columns.Add("EloquaFieldName");
                //        foreach (var row as DataRow in ds.Tables[0].Rows)
                //            {
                //dt.Rows.Add(new object[] { row["name"], row["internalName"]}
                //            }

                var rowcount = ds.Tables[0].Rows.Count;
                var columncount = ds.Tables[0].Columns.Count;

                for (int r = 0; r < rowcount; r++)
                {
                    dt.Rows.Add(new object[] { ds.Tables[0].Rows[r]["name"].ToString(), ds.Tables[0].Rows[r]["internalName"].ToString() });
                }

                dataset.Tables.Add(dt);
                //    if (InstanceId == 4)
                //{
                //    ds.Tables[0].Columns.RemoveAt(2); ds.Tables[0].Columns.RemoveAt(3); ds.Tables[0].Columns.RemoveAt(4); ds.Tables[0].Columns.RemoveAt(5); ds.Tables[0].Columns.RemoveAt(6);
                //    ds.Tables[0].Columns.RemoveAt(2); ds.Tables[0].Columns.RemoveAt(3); ds.Tables[0].Columns.RemoveAt(2); ds.Tables[0].Columns.RemoveAt(3);
                //    ds.Tables[0].Columns.RemoveAt(2);
                //    if(TableName=="contacts")
                //    ds.Tables[0].Columns.RemoveAt(2);
                //}
                //else
                //{   ds.Tables[0].Columns.RemoveAt(2); ds.Tables[0].Columns.RemoveAt(3); ds.Tables[0].Columns.RemoveAt(4); ds.Tables[0].Columns.RemoveAt(5); ds.Tables[0].Columns.RemoveAt(6);
                //    ds.Tables[0].Columns.RemoveAt(7); ds.Tables[0].Columns.RemoveAt(2); ds.Tables[0].Columns.RemoveAt(3); ds.Tables[0].Columns.RemoveAt(2); ds.Tables[0].Columns.RemoveAt(3);
                //    ds.Tables[0].Columns.RemoveAt(2);
                //}
                Console.WriteLine(TableName + " Fields Exported to " + TableName + "_fields");
                Console.WriteLine("================================================================================");

                dataset.Tables[0].TableName = "TableFields";
                balMain.Insert_TableFields(dataset, InstanceId, TableName);
            }
            catch (Exception e)
            {
                Console.WriteLine("Field Creation Error :" + e);
                // goto TryAgain;
            }
        }

        public void Get_CustomObjects(int InstanceId)
        {
            DataTable dtCustomObjects = new DataTable();
            dtCustomObjects.Columns.Add("CustomObjectId");
            dtCustomObjects.Columns.Add("InstanceId");
            dtCustomObjects.Columns.Add("Name");
            dtCustomObjects.Columns.Add("displayNameFieldUri");
            dtCustomObjects.Columns.Add("emailAddressFieldUri");
            dtCustomObjects.Columns.Add("uniqueFieldUri");
            dtCustomObjects.Columns.Add("uri");
            dtCustomObjects.Columns.Add("CreatedBy");
            dtCustomObjects.Columns.Add("CreatedAt", typeof(DateTime));
            dtCustomObjects.Columns.Add("UpdatedBy");
            dtCustomObjects.Columns.Add("UpdatedAt", typeof(DateTime));
            Console.WriteLine("================================================================================");
            Console.WriteLine("Export Started");

            //mm string Authentication = "Basic " + GetCredential();
            var client = new RestClient(EloquaUrl);//"https://secure.eloqua.com/api/bulk/2.0"

            var request = new RestRequest("/customObjects", Method.GET);
            request.AddParameter("Authorization", Authentication, ParameterType.HttpHeader);
            var response = client.Execute(request);
            //WriteToFile(response.Content);
            var result = JsonConvert.DeserializeObject<dynamic>(response.Content);
            string updatedBy = "", createdBy = "", uri = "", name = "", uniqueFieldUri = "";
            DateTime createdAt = new DateTime();
            DateTime updatedAt = new DateTime();
            for (int i = 0; i < result.items.Count; i++)
            {
                IDictionary<string, JToken> dict = result.items[i];
                string[] customobject = result.items[i]["uri"].Value.Split('/');
                string displayNameFieldUri = result.items[i]["displayNameFieldUri"] == null ? "" : result.items[i]["displayNameFieldUri"].Value;
                string emailAddressFieldUri = result.items[i]["emailAddressFieldUri"] == null ? "" : result.items[i]["emailAddressFieldUri"].Value;
                if (dict.ContainsKey("uniqueFieldUri"))
                    uniqueFieldUri = result.items[i]["uniqueFieldUri"] == null ? "" : result.items[i]["uniqueFieldUri"].Value;

                if (dict.ContainsKey("name"))
                    name = result.items[i]["name"] == null ? "" : result.items[i]["name"].Value;
                if (dict.ContainsKey("uri"))
                    uri = result.items[i]["uri"] == null ? "" : result.items[i]["uri"].Value;

                if (dict.ContainsKey("createdBy"))
                    createdBy = result.items[i]["createdBy"] == null ? "" : result.items[i]["createdBy"].Value;
                if (dict.ContainsKey("createdAt"))
                    createdAt = result.items[i]["createdAt"] == null ? "" : result.items[i]["createdAt"].Value;
                if (dict.ContainsKey("updatedBy"))
                    updatedBy = result.items[i]["updatedBy"] == null ? "" : result.items[i]["updatedBy"].Value;
                if (dict.ContainsKey("updatedAt"))
                    updatedAt = result.items[i]["updatedAt"] == null ? "" : result.items[i]["updatedAt"].Value;
                // dtCustomObjects.Rows.Add(customobject[2], InstanceId, result.items[i]["name"].Value, displayNameFieldUri, emailAddressFieldUri, uniqueFieldUri, result.items[i]["uri"].Value, result.items[i]["createdBy"].Value, result.items[i]["createdAt"].Value, result.items[i]["updatedBy"].Value, result.items[i]["updatedAt"].Value);
                dtCustomObjects.Rows.Add(customobject[2], InstanceId, name, displayNameFieldUri, emailAddressFieldUri, uniqueFieldUri, uri, createdBy, createdAt, updatedBy, updatedAt);
            }
            BAL_Main objGeneral = new BAL_Main();
            string resultval = objGeneral.InsertCustomObject(dtCustomObjects, InstanceId);

            if (resultval.ToLower().Contains(logname.ToLower())) //Maduraiveeran
            {
                // balMain.SendEmail();
                Environment.Exit(0);
                //High Priority Mail Send *******************//
            }
            else
            {
                Console.Write(result.ToString());
                Console.WriteLine("Data Exported");
                Console.WriteLine("================================================================================");
            }
        }

        public void Insert_CustomObjectFields(int InstanceId)
        {
            DataTable dtCustomObjects = new DataTable();
            DataTable dtCustomFields = new DataTable();
            BAL_Main objGeneral = new BAL_Main();
            dtCustomFields.Columns.Add("CustomObjectID");
            dtCustomFields.Columns.Add("FieldId");
            dtCustomFields.Columns.Add("name");
            dtCustomFields.Columns.Add("internalName");
            dtCustomFields.Columns.Add("dataType");
            dtCustomFields.Columns.Add("statement");
            dtCustomFields.Columns.Add("uri");
            dtCustomFields.Columns.Add("instanceId");
            dtCustomObjects = objGeneral.SelectCustomObjects(InstanceId);
            foreach (DataRow dr in dtCustomObjects.Rows)
            {
                string url = dr["uri"].ToString() + "/fields";
                string CustomObjectId = dr["CustomObjectId"].ToString();
                //mm string Authentication = "Basic " + GetCredential();
                var client = new RestClient(EloquaUrl);

                var request = new RestRequest(url, Method.GET);
                request.AddParameter("Authorization", Authentication, ParameterType.HttpHeader);
                var response = client.Execute(request);
                //WriteToFile(response.Content);
                var result = JsonConvert.DeserializeObject<dynamic>(response.Content);
                string statement = "", name = "", internalName = "", dataType = "";
                for (int i = 0; i < result.items.Count; i++)
                {
                    IDictionary<string, JToken> dict = result.items[i];
                    string uri = result.items[i]["uri"].Value;
                    string[] strUri = uri.Split('/');
                    string strUrival = "";
                    if (uri == "" || uri == null) { uri = ""; strUrival = ""; } else { strUrival = strUri[4]; }

                    if (dict.ContainsKey("statement"))
                        statement = result.items[i]["statement"] == null ? "" : result.items[i]["statement"].Value;
                    if (dict.ContainsKey("name"))
                        name = result.items[i]["name"] == null ? "" : result.items[i]["name"].Value;
                    if (dict.ContainsKey("internalName"))
                        internalName = result.items[i]["internalName"] == null ? "" : result.items[i]["internalName"].Value;
                    if (dict.ContainsKey("dataType"))
                        dataType = result.items[i]["dataType"] == null ? "" : result.items[i]["dataType"].Value;
                    //dtCustomFields.Rows.Add(CustomObjectId, strUri[4], result.items[i]["name"].Value, result.items[i]["internalName"].Value, result.items[i]["dataType"].Value, result.items[i]["statement"].Value, result.items[i]["uri"].Value, InstanceId);
                    //dtCustomFields.Rows.Add(CustomObjectId, strUrival, result.items[i]["name"].Value, result.items[i]["internalName"].Value, result.items[i]["dataType"].Value, statement, uri, InstanceId);
                    dtCustomFields.Rows.Add(CustomObjectId, strUrival, name, internalName, dataType, statement, uri, InstanceId);
                }
            }
            string resultval = objGeneral.Insert_SelectCustomObjectFields(dtCustomFields, InstanceId);
            if (resultval.ToLower().Contains(logname.ToLower())) //Maduraiveeran
            {
                // balMain.SendEmail();
                Environment.Exit(0);
                //High Priority Mail Send *******************//
            }
        }

        public void ExportFromEloquamanual(int Id, int InstanceId, string TableName, string Url, string InstanceName)
        {
            int TableId = 1; int offset = 0; int RecordLimit = 0;
            TableId = balMain.GetObjectId(TableName);
            DataTable dtExportUrl = new DataTable();
            dtExportUrl.Columns.Add("ExportUrl");
            dtExportUrl.Columns.Add("SyncUrl");
            dtExportUrl.Columns.Add("CreatedDate");
            DataSet dsExport = new DataSet();
            switch (TableName)
            {
                case "CustomObjectData":
                case "CustomObject":
                    string[] firsturl = Url.Split('|');
                    string[] combined1 = firsturl[0].Split('&');
                    offset = Convert.ToInt32(combined1[0].Split('?')[1].Split('=')[1]);//Convert.ToInt32(combined1[3].Split('=')[1]);
                    RecordLimit = Convert.ToInt32(combined1[1].Split('=')[1]);//Convert.ToInt32(combined1[4].Split('=')[1]);
                    string urlval = "";//"/syncs/" + combined1[0].Split('/')[2];
                    if (combined1[0].Split('/').Length > 1)
                    {
                        urlval = "/syncs/" + combined1[0].Split('/')[2];
                    }
                    else
                    {
                        urlval = "/syncs/" + combined1[0].Split('/')[1];
                    }

                    string CustomObjectId = firsturl[1].ToString();
                    DataTable dtCustomObjectFields = new DataTable();
                    DataSet ds = balMain.GetCustomObjectandFieldsRerunurl(InstanceId, CustomObjectId);
                    dtCustomObjectFields = ds.Tables[0];
                    get_CustomObjectExportDara(urlval, CustomObjectId, Authentication, dtCustomObjectFields, InstanceId, 1);
                    break;

                case "contacts":
                case "Contact":
                    if (Url.Trim() == "" || Url.Trim() == null)
                    {
                        balMain.insertcontacttemp_To_ContactTable(InstanceId, InstanceName); //This is insert temp table to contact and drop temp table
                    }
                    else
                    {
                        TableName = "contacts";
                        int RecordCount = 0, RecordLoopCount = 0; RecordLimit = 100;
                        int FieldLimit = 99;
                        int FieldCount = GetFieldCount(TableName, InstanceId) + 1; //Find Total record count contact(244)
                        int FieldLoopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(FieldCount) / FieldLimit));

                        string[] combined = Url.Replace(" ", "").Replace("&&", "&").Split('&');// 105830 & 105831 & 105832 & &offset=0&limit=5000
                        dtExportUrl.Rows.Add("", "syncs/" + combined[0], DateTime.Now.ToString("yyyyMMdd"));
                        dtExportUrl.Rows.Add("", "syncs/" + combined[1], DateTime.Now.ToString("yyyyMMdd"));
                        dtExportUrl.Rows.Add("", "syncs/" + combined[2], DateTime.Now.ToString("yyyyMMdd"));
                        offset = Convert.ToInt32(combined[3].Split('=')[1]);
                        RecordLimit = Convert.ToInt32(combined[4].Split('=')[1]);
                        string[] SyncUrl1 = new string[dtExportUrl.Rows.Count];

                        CreateContact_TempTable_and_Procedure(InstanceId, InstanceName);  // This is create Temp Table and Procedure
                        int innrloop = 0;
                        if (dtExportUrl.Rows.Count > 0)
                        {
                            bool SyncFlag = true;
                            foreach (DataRow dr in dtExportUrl.Rows)
                            {
                            Recheck:

                                string Status = Check_SyncStatus(dr["SyncUrl"].ToString(), Authentication);
                                if (Status == "error" || Status == "Error")
                                {
                                    string urls = dr["SyncUrl"].ToString();
                                    balMain.insertTransationRecord(TableName, InstanceId, "Object Error", urls, "Sync Error");
                                    break;
                                }
                                else if (Status != "success")
                                {
                                    SyncFlag = false;
                                    Thread.Sleep(6000);
                                    goto Recheck;
                                }
                                else { SyncUrl1[innrloop] = dr["SyncUrl"].ToString(); innrloop += 1; SyncFlag = true; }
                            }
                            //balMain.InsertObjectHistory(InstanceId, TableId, "SyncEnd");  //Sync End

                            if (SyncFlag)
                            {
                                string syncurls = "";

                                int totalinsert = 0;
                                try
                                {
                                    string response = Get_ExportData(dtExportUrl.Rows[0][1].ToString(), 0, 1, Authentication, TableName);
                                    var result = JsonConvert.DeserializeObject<dynamic>(response);
                                    RecordCount = Convert.ToInt32(result.totalResults.Value);
                                    RecordLimit = balMain.GetRecordLimit(TableName);
                                    if (RecordCount > RecordLimit) RecordLoopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(RecordCount) / RecordLimit));
                                    else if (RecordCount == 0)
                                    {
                                        string url = syncurls + "&offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                        //balMain.InsertObjectHistory(InstanceId, TableId, "Exportstart");  //Export Start
                                        balMain.insertTransationRecord(TableName, InstanceId, "Transation", url, "Inserted = 0");
                                        balMain.insertrecordcount(TableName, RecordCount, 0, InstanceId);
                                        goto End;
                                    }
                                    else RecordLoopCount = 1;
                                    // balMain.InsertObjectHistory(InstanceId, TableId, "Exportstart");  //Export Start
                                    totalinsert = 0;
                                    for (int i = 1; i <= RecordLoopCount; i++)
                                    {
                                        DataSet dsExport1 = new DataSet();
                                        dsExport.Dispose();
                                        syncurls = "";
                                        if (i == 1) { offset = 0; }
                                        else { offset = offset + RecordLimit; }
                                        string[] datasettablename = { "contacts", "contacts1", "contacts2" };
                                        int s = 0;
                                        for (int mm = 0; mm < dtExportUrl.Rows.Count; mm++)
                                        {
                                            syncurls += SyncUrl1[mm].Split('/')[1].ToString() + " & ";
                                            string EloquaData = Get_ExportData(SyncUrl1[mm].ToString(), offset, RecordLimit, Authentication, TableName);
                                            List<string> res = EloquaData.Split(',').ToList();
                                            int totalResults = Convert.ToInt32(res[0].Split(':')[1].ToString());//{"totalResults":0
                                            if (totalResults == 0)
                                            {
                                                string urls = syncurls + "offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                                balMain.insertTransationRecord(TableName, InstanceId, "Transation", urls, "Inserted = 0");
                                            }
                                            else
                                            {
                                                if (EloquaData == null || EloquaData == "null" || EloquaData == "NULL" || EloquaData == "")
                                                {
                                                    if (s <= 2)
                                                    {
                                                        //var synurl = SyncUrl1[mm].ToString(); var offfset = offset; var recordlimt = RecordLimit; var authn = Authentication; var tablenname = TableName;
                                                        if (s == 2)
                                                        {
                                                            string urls = syncurls + "offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                                            balMain.insertTransationRecord(TableName, InstanceId, "Error", urls, "Null Data");
                                                        }
                                                        mm -= 1;
                                                    }

                                                    s += 1;
                                                }
                                                else
                                                {
                                                    s = 0;

                                                    dsExport1.Tables.Add(JsonConvert.DeserializeObject<DataTable>(JsonConvert.DeserializeObject<dynamic>(EloquaData).items.ToString()));
                                                    dsExport1.Tables[mm].TableName = datasettablename[mm]; //dsExport.Tables[cnt - 1].TableName = "Subscribe";//dr["TableName"].ToString();
                                                    var rowcount = dsExport1.Tables[mm].Rows.Count;
                                                    var columncount = dsExport1.Tables[mm].Columns.Count;

                                                    for (int r = 0; r < rowcount; r++)
                                                    {
                                                        for (int c = 0; c < columncount; c++)
                                                        {
                                                            if (dsExport1.Tables[mm].Rows[r][c].ToString() == "") { dsExport1.Tables[mm].Rows[r][c] = null; }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        DataTable FinalXMLTable = new DataTable();
                                        string resultval = balMain.Insert_Update_Contacts(dsExport1, InstanceId, FinalXMLTable);

                                        if (resultval.ToLower().Contains(logname.ToLower())) //Maduraiveeran
                                        {
                                            //balMain.SendEmail();//High Priority Mail Send *******************//
                                            Environment.Exit(0);
                                        }
                                        else
                                        {
                                            int inserted = 0;
                                            if (resultval == "") { totalinsert += 0; }
                                            else
                                            {
                                                inserted = Convert.ToInt32(resultval);
                                                totalinsert += Convert.ToInt32(resultval);
                                            }
                                            string url = syncurls + "&offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                            balMain.insertTransationRecord(TableName, InstanceId, "Transation", url, "Inserted = " + inserted + " & from Eloqua : " + dsExport1.Tables[0].Rows.Count);
                                            dsExport1.Dispose();
                                        }
                                    }
                                    balMain.insertrecordcount(TableName, RecordCount, totalinsert, InstanceId); //To insert Total records for eloqua and table

                                End:
                                    totalinsert = 0;
                                }
                                catch (Exception e)
                                {
                                    var synurl = SyncUrl1[0].Split('/')[2].ToString() + " & " + SyncUrl1[1].Split('/')[2].ToString() + " & " + SyncUrl1[2].Split('/')[2].ToString() + " & ";
                                    string url = synurl + "&offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString(); ;
                                    balMain.insertTransationRecord(TableName, InstanceId, "Error", url, e.ToString());
                                }
                            }
                        }

                        balMain.insertcontacttemp_To_ContactTable(InstanceId, InstanceName);
                    }

                    #region Old Code

                    //    string[] combined = Url.Replace(" ", "").Replace("&&", "&").Split('&');// 105830 & 105831 & 105832 & &offset=0&limit=5000
                    //dtExportUrl.Rows.Add("", "syncs/" + combined[0], DateTime.Now.ToString("yyyyMMdd"));
                    //dtExportUrl.Rows.Add("", "syncs/" + combined[1], DateTime.Now.ToString("yyyyMMdd"));
                    //dtExportUrl.Rows.Add("", "syncs/" + combined[2], DateTime.Now.ToString("yyyyMMdd"));
                    //offset = Convert.ToInt32(combined[3].Split('=')[1]);
                    //RecordLimit = Convert.ToInt32(combined[4].Split('=')[1]);
                    //string[] SyncUrl1 = new string[dtExportUrl.Rows.Count];
                    //SyncUrl1[0] = "syncs/" + combined[0];
                    //SyncUrl1[1] = "syncs/" + combined[1];
                    //SyncUrl1[2] = "syncs/" + combined[2];

                    //if (dtExportUrl.Rows.Count > 0)
                    //{
                    //    string syncurls = "";
                    //    balMain.InsertObjectHistoryRerun(InstanceId, TableId, "Exportstart");  //Export Start
                    //    int totalinsert = 0;
                    //    try
                    //    {
                    //        string[] datasettablename = { "contacts", "contacts1", "contacts2" };
                    //        for (int mm = 0; mm < dtExportUrl.Rows.Count; mm++)
                    //        {
                    //            int s = 0;
                    //            if (SyncUrl1[mm].Split('/').Count() > 2)
                    //            {
                    //                syncurls += SyncUrl1[mm].Split('/')[2].ToString() + " & ";
                    //            }
                    //            else
                    //            {
                    //                syncurls += SyncUrl1[mm].ToString() + " & ";
                    //            }
                    //            string EloquaData = Get_ExportData(SyncUrl1[mm].ToString(), offset, RecordLimit, Authentication, TableName);
                    //            List<string> reslt = EloquaData.Split(',').ToList();
                    //            int totalResults = Convert.ToInt32(reslt[0].Split(':')[1].ToString());//{"totalResults":0
                    //            if (totalResults == 0)
                    //            {
                    //                string urls = syncurls + "offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString(); ;
                    //                balMain.insertTransationRecord(TableName, InstanceId, "Transation", urls.Replace("syncs/", ""), "Rerun Inserted = 0");
                    //            }
                    //            else
                    //            {
                    //                if (EloquaData == null || EloquaData == "null" || EloquaData == "NULL" || EloquaData == "")
                    //                {
                    //                    if (s <= 2)
                    //                    {
                    //                        if (s == 2)
                    //                        {
                    //                            string urls = syncurls + "offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                    //                            balMain.insertTransationRecord(TableName, InstanceId, "Error - ReRun", urls.Replace("syncs/", ""), "Null Data");
                    //                        }
                    //                        mm -= 1;
                    //                    }
                    //                }
                    //                else
                    //                {
                    //                    dsExport.Tables.Add(JsonConvert.DeserializeObject<DataTable>(JsonConvert.DeserializeObject<dynamic>(EloquaData).items.ToString()));
                    //                    dsExport.Tables[mm].TableName = datasettablename[mm];
                    //                    var rowcount = dsExport.Tables[mm].Rows.Count;
                    //                    var columncount = dsExport.Tables[mm].Columns.Count;
                    //                    for (int r = 0; r < rowcount; r++)
                    //                    {
                    //                        for (int c = 0; c < columncount; c++)
                    //                        {
                    //                            if (dsExport.Tables[mm].Rows[r][c].ToString() == "") { dsExport.Tables[mm].Rows[r][c] = null; }
                    //                        }
                    //                    }
                    //                }
                    //            }
                    //        }
                    //        string resultval = balMain.Insert_Update_Contacts(dsExport, InstanceId);
                    //        if (resultval.ToLower().Contains(logname.ToLower())) //Maduraiveeran
                    //        {
                    //            // balMain.SendEmail();
                    //            Environment.Exit(0);
                    //            //High Priority Mail Send *******************//
                    //        }
                    //        else
                    //        {
                    //            int inserted = 0;
                    //            if (resultval == "") { totalinsert = 0; }
                    //            else
                    //            {
                    //                inserted = Convert.ToInt32(resultval);
                    //                totalinsert = Convert.ToInt32(resultval);
                    //            }

                    //            string url = syncurls + "offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                    //            balMain.insertTransationRecord(TableName, InstanceId, "Transation", url.Replace("syncs/", ""), "ReInserted = " + inserted);
                    //            // balMain.updateRerunstatus(Id,InstanceId, TableName, Url);
                    //            dsExport.Dispose();
                    //        }

                    //    }
                    //    catch (Exception e)
                    //    {
                    //        var synurl = SyncUrl1[0].Split('/')[2].ToString() + " & " + SyncUrl1[1].Split('/')[2].ToString() + " & " + SyncUrl1[2].Split('/')[2].ToString() + " & ";
                    //        string errurl = synurl + "&offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString(); ;
                    //        balMain.insertTransationRecord(TableName, InstanceId, "Error", errurl, e.ToString());
                    //    }
                    //}
                    //balMain.InsertObjectHistoryRerun(InstanceId, TableId, "ExpEnd");  //Export End Record Insert End

                    #endregion Old Code

                    break;

                default:
                    dtExportUrl.Rows.Add("", Url, DateTime.Now.ToString("yyyyMMdd"));
                    int totalinsertval = 0;
                    // balMain.InsertObjectHistoryRerun(InstanceId, TableId, "Exportstart");  //Export Start

                    try
                    {
                        string EloquaData = Get_ExportDatamanual(Url, Authentication, TableName);
                        List<string> result = EloquaData.Split(',').ToList();
                        int totalResults = Convert.ToInt32(result[0].Split(':')[1].ToString());//{"totalResults":0
                        if (totalResults == 0)
                        {
                            balMain.insertTransationRecord(TableName, InstanceId, "Transation", Url, "ReInserted = 0");
                        }
                        else
                        {
                            if (EloquaData == null || EloquaData == "null" || EloquaData == "NULL" || EloquaData == "")
                            {
                                balMain.insertTransationRecord(TableName, InstanceId, "Error - ReRun", Url, "Null Data");
                            }
                            else
                            {
                                dsExport.Tables.Add(JsonConvert.DeserializeObject<DataTable>(JsonConvert.DeserializeObject<dynamic>(EloquaData).items.ToString()));
                                dsExport.Tables[0].TableName = "Accounts";
                                var rowcount = dsExport.Tables[0].Rows.Count;
                                var columncount = dsExport.Tables[0].Columns.Count;
                                for (int r = 0; r < rowcount; r++)
                                {
                                    for (int c = 0; c < columncount; c++)
                                    {
                                        if (dsExport.Tables[0].Rows[r][c].ToString() == "") { dsExport.Tables[0].Rows[r][c] = null; }
                                    }
                                }
                                string resultval = "";
                                if (TableName == "accounts") resultval = balMain.Insert_Update_Accounts(dsExport.Tables[0], InstanceId);
                                else resultval = balMain.Insert_Activities(dsExport.Tables[0], InstanceId, TableName);
                                if (resultval.ToLower().Contains(logname.ToLower())) //Maduraiveeran
                                {
                                    //  balMain.SendEmail();
                                    Environment.Exit(0);
                                    //High Priority Mail Send *******************//
                                }
                                else
                                {
                                    int inserted = 0;
                                    if (resultval == "") { totalinsertval = 0; }
                                    else
                                    {
                                        inserted = Convert.ToInt32(resultval);
                                        totalinsertval = Convert.ToInt32(resultval);
                                    }
                                    balMain.updateTotalrecordinsert(Id, TableName, InstanceId, inserted);
                                    balMain.insertTransationRecord(TableName, InstanceId, "Transation", Url, "ReInserted = " + inserted);
                                    // balMain.updateRerunstatus(Id, InstanceId, TableName, Url);
                                    dsExport.Dispose();
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        balMain.insertTransationRecord(TableName, InstanceId, "Error - ReRun", Url, e.ToString());
                    }

                    // balMain.InsertObjectHistoryRerun(InstanceId, TableId, "ExpEnd");  //Export End Record Insert End
                    break;
            }
            balMain.UpdateTransactionLogTable(Id, InstanceId);  //Export End Record Insert End
        }

        public static string Get_ExportDatamanual(string Url, string Authentication, string TableName)
        {
            Console.WriteLine("==================== " + TableName + " Export Started ====================");
            Console.WriteLine();
            Console.WriteLine("Exporting data from " + Url);
            Console.WriteLine();
            var client = new RestClient(EloquaUrl);
            var request = new RestRequest(Url, Method.GET);
            request.AddParameter("Authorization", Authentication, ParameterType.HttpHeader);
            var response = client.Execute(request);
            return response.Content;
        }

        public int GetFieldscount(string TableName, int InstanceId)
        {
            var client = new RestClient(EloquaUrl);
            var request = new RestRequest("/" + TableName + "/fields/", Method.GET);
            request.AddParameter("Authorization", Authentication, ParameterType.HttpHeader);
            var response = client.Execute(request);
            var result = JsonConvert.DeserializeObject<dynamic>(response.Content);
            int totalResults = JsonConvert.DeserializeObject<int>(JsonConvert.DeserializeObject<dynamic>(response.Content).totalResults.ToString());
            int total = balMain.TableColumncount(TableName, InstanceId);
            if ((totalResults + 1) == total) { return 1; }
            else { return 0; }
        }

        //public void Insert_FormFields(int InstanceId)
        //{
        //    try
        //    {
        //        DataTable dtRawData = new DataTable();
        //        DataTable dtFormFields = new DataTable();
        //        dtFormFields.Columns.Add("ActivityId", typeof(int));
        //        dtFormFields.Columns.Add("FieldName", typeof(string));
        //        dtFormFields.Columns.Add("FieldValue", typeof(string));
        //        dtRawData = balMain.Get_RawData(InstanceId);
        //        foreach (DataRow dr in dtRawData.Rows)
        //        {
        //            string RawData = dr["RawData"].ToString();
        //            string[] RawDataFields = RawData.Split('&');
        //            foreach (string value in RawDataFields)
        //            {
        //                string[] FieldName = value.Split('=');
        //                string FieldValue = FieldName[1].Replace("%20", " ").Replace("%40", "@").Replace("%2b", "+").Replace("%26", "&").Replace("%3a", ":").Replace("%2f", "/");
        //                dtFormFields.Rows.Add(dr["ActivityId"].ToString(), FieldName[0], FieldValue);
        //            }
        //        }
        //        balMain.Insert_FormField(dtFormFields, InstanceId);
        //    }
        //    catch (Exception e)
        //    {
        //        balMain.insertTransationRecord("FormField", InstanceId, "Error", "Insert Error", e.ToString());
        //    }

        //}

        public void ExportFromEloquapartTable(string TableName, int InstanceId, int TableId, string ErrorExportUrls, int ErrorlogID)
        {
            DataTable dtExportUrl = new DataTable();
            dtExportUrl.Columns.Add("ExportUrl");
            dtExportUrl.Columns.Add("SyncUrl");
            dtExportUrl.Columns.Add("CreatedDate");
            DataTable dt = new DataTable();
            int RecordCount = 0, RecordLoopCount = 0, RecordLimit = 1000;
            int FieldLimit = 99;
            int FieldCount = GetFieldCount(TableName, InstanceId) + 1; //Find Total record count contact(244)
            int FieldLoopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(FieldCount) / FieldLimit)); // Loop iteration

            string ExportUrls; string SyncUrl;

            balMain.InsertObjectHistory(InstanceId, TableId, "SyncStart");  //Insert Sync Start
            if (TableName == "contacts")   //Contact Table
            {
                for (int i = 1; i <= FieldLoopCount; i++)
                {
                    ExportUrls = Create_Export(TableName, i, Authentication, InstanceId, ErrorlogID, 0);
                    SyncUrl = Sync_Export(ExportUrls, Authentication);
                    dtExportUrl.Rows.Add(ExportUrls, SyncUrl, DateTime.Now.ToString("yyyyMMdd"));
                }

                string[] SyncUrl1 = new string[dtExportUrl.Rows.Count];
                int innrloop = 0; int offset = 0;
                if (dtExportUrl.Rows.Count > 0)
                {
                    bool SyncFlag = true;
                    foreach (DataRow dr in dtExportUrl.Rows)
                    {
                    Recheck:

                        string Status = Check_SyncStatus(dr["SyncUrl"].ToString(), Authentication);
                        if (Status == "error" || Status == "Error")
                        {
                            string urls = dr["SyncUrl"].ToString();
                            balMain.insertTransationRecord(TableName, InstanceId, "Object Error", urls, "Sync Error ReRun");
                            break;
                        }
                        else if (Status != "success")
                        {
                            SyncFlag = false;
                            Thread.Sleep(6000);
                            goto Recheck;
                        }
                        else { SyncUrl1[innrloop] = dr["SyncUrl"].ToString(); innrloop += 1; SyncFlag = true; }
                    }
                    balMain.InsertObjectHistory(InstanceId, TableId, "SyncEnd");  //Sync End

                    if (SyncFlag)
                    {
                        string syncurls = "";

                        int totalinsert = 0;
                        try
                        {
                            string response = Get_ExportData(dtExportUrl.Rows[0][1].ToString(), 0, 1, Authentication, TableName);
                            var result = JsonConvert.DeserializeObject<dynamic>(response);
                            RecordCount = Convert.ToInt32(result.totalResults.Value);
                            RecordLimit = balMain.GetRecordLimit(TableName);
                            if (RecordCount > RecordLimit) RecordLoopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(RecordCount) / RecordLimit));
                            else if (RecordCount == 0)
                            {
                                string url = syncurls + "&offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                balMain.insertTransationRecord(TableName, InstanceId, "Object Rerun Transation", url, "Inserted = 0");
                                balMain.insertrecordcount(TableName, RecordCount, 0, InstanceId);
                                goto End;
                            }
                            else RecordLoopCount = 1;
                            balMain.InsertObjectHistory(InstanceId, TableId, "Exportstart");  //Export Start
                            totalinsert = 0;
                            for (int i = 1; i <= RecordLoopCount; i++)
                            {
                                syncurls = "";
                                DataSet dsExport = new DataSet();
                                if (i == 1) { offset = 0; }
                                else { offset = offset + RecordLimit; }
                                string[] datasettablename = { "contacts", "contacts1", "contacts2" };
                                int s = 0;
                                for (int mm = 0; mm < dtExportUrl.Rows.Count; mm++)
                                {
                                    syncurls += SyncUrl1[mm].Split('/')[2].ToString() + " & ";
                                    string EloquaData = Get_ExportData(SyncUrl1[mm].ToString(), offset, RecordLimit, Authentication, TableName);

                                    List<string> res = EloquaData.Split(',').ToList();
                                    int totalResults = Convert.ToInt32(res[0].Split(':')[1].ToString());//{"totalResults":0
                                    if (totalResults == 0)
                                    {
                                        string urls = syncurls + "offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                        balMain.insertTransationRecord(TableName, InstanceId, "Transation", urls, "Object Rerun Inserted = 0");
                                    }
                                    else
                                    {
                                        if (EloquaData == null || EloquaData == "null" || EloquaData == "NULL" || EloquaData == "")
                                        {
                                            if (s <= 2)
                                            {
                                                if (s == 2)
                                                {
                                                    string urls = syncurls + "offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                                    balMain.insertTransationRecord(TableName, InstanceId, "Error", urls, "Object Rerun Null Data");
                                                }
                                                mm -= 1;
                                            }

                                            s += 1;
                                        }
                                        else
                                        {
                                            s = 0;
                                            dsExport.Tables.Add(JsonConvert.DeserializeObject<DataTable>(JsonConvert.DeserializeObject<dynamic>(EloquaData).items.ToString()));
                                            dsExport.Tables[mm].TableName = datasettablename[mm]; //dsExport.Tables[cnt - 1].TableName = "Subscribe";//dr["TableName"].ToString();
                                            var rowcount = dsExport.Tables[mm].Rows.Count;
                                            var columncount = dsExport.Tables[mm].Columns.Count;

                                            for (int r = 0; r < rowcount; r++)
                                            {
                                                for (int c = 0; c < columncount; c++)
                                                {
                                                    if (dsExport.Tables[mm].Rows[r][c].ToString() == "") { dsExport.Tables[mm].Rows[r][c] = null; }
                                                }
                                            }
                                        }
                                    }
                                }
                                DataTable FinalXMLTable = new DataTable();
                                string resultval = balMain.Insert_Update_Contacts(dsExport, InstanceId, FinalXMLTable);
                                if (resultval.ToLower().Contains(logname.ToLower())) //Maduraiveeran
                                {
                                    // balMain.SendEmail();
                                    Environment.Exit(0);
                                    //High Priority Mail Send *******************//
                                }
                                else
                                {
                                    int inserted = 0;
                                    if (resultval == "") { totalinsert += 0; }
                                    else
                                    {
                                        inserted = Convert.ToInt32(resultval);
                                        totalinsert += Convert.ToInt32(resultval);
                                    }
                                    string url = syncurls + "&offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                    balMain.insertTransationRecord(TableName, InstanceId, "Object Rerun Transation", url, "Inserted = " + inserted);
                                    dsExport.Dispose();
                                }
                            }
                            balMain.insertrecordcount(TableName, RecordCount, totalinsert, InstanceId); //To insert Total records for eloqua and table

                        End:
                            totalinsert = 0;
                            //}
                        }
                        catch (Exception e)
                        {
                            var synurl = SyncUrl1[0].Split('/')[2].ToString() + " & " + SyncUrl1[1].Split('/')[2].ToString() + " & " + SyncUrl1[2].Split('/')[2].ToString() + " & ";
                            string url = synurl + "&offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString(); ;
                            balMain.insertTransationRecord(TableName, InstanceId, "Error", url, e.ToString());
                        }
                    }
                }
                balMain.InsertObjectHistory(InstanceId, TableId, "ExpEnd");  //Export End Record Insert End
            }
            else
            {
                if (ErrorExportUrls.Contains("data"))
                {
                    ErrorExportUrls = ErrorExportUrls.Split('?')[0].Replace("/data", "");
                }

                dtExportUrl.Rows.Clear();
                dtExportUrl.Rows.Add("", ErrorExportUrls, DateTime.Now.ToString("yyyyMMdd"));
                int totalinsert = 0;
                if (dtExportUrl.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtExportUrl.Rows)
                    {
                    Recheck:
                        bool SyncFlag = true;
                        string Status = Check_SyncStatus(dr["SyncUrl"].ToString(), Authentication);
                        if (Status == "error" || Status == "Error")
                        {
                            string urls = dr["SyncUrl"].ToString();
                            balMain.insertTransationRecord(TableName, InstanceId, "Object Error", urls, "Sync Error");
                            break;
                        }
                        else if (Status != "success")
                        {
                            SyncFlag = false;
                            Thread.Sleep(6000);
                            goto Recheck;
                        }

                        balMain.InsertObjectHistory(InstanceId, TableId, "SyncEnd");  //Sync End
                        if (SyncFlag)
                        {
                            string response = Get_ExportData(dtExportUrl.Rows[0][1].ToString(), 0, 1, Authentication, TableName);
                            var result = JsonConvert.DeserializeObject<dynamic>(response);
                            RecordCount = Convert.ToInt32(result.totalResults.Value);
                            RecordLimit = balMain.GetRecordLimit(TableName);
                            if (RecordCount > RecordLimit) RecordLoopCount = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(RecordCount) / RecordLimit));
                            else if (RecordCount == 0)
                            {
                                string url = dtExportUrl.Rows[0][1].ToString() + "/data?offset=" + 0 + "&limit=" + RecordLimit.ToString();
                                balMain.insertTransationRecord(TableName, InstanceId, "Transation", url, "Inserted = 0");
                                balMain.insertrecordcount(TableName, RecordCount, 0, InstanceId);
                                break;
                            }
                            else RecordLoopCount = 1;
                            //RecordLoopCount = 1;
                            balMain.InsertObjectHistory(InstanceId, TableId, "Exportstart");  //Export Start
                            int offset = 0; totalinsert = 0;
                            for (int i = 1; i <= RecordLoopCount; i++)
                            {
                                if (i == 1) { offset = 0; }
                                else { offset = offset + RecordLimit; }
                                string[] datasettablename = { "contacts", "contacts1", "contacts2" };

                                DataSet dsExport = new DataSet();
                                int s = 0;
                                for (int mm = 0; mm < dtExportUrl.Rows.Count; mm++)
                                {
                                    try
                                    {
                                        string EloquaData = Get_ExportData(dr["SyncUrl"].ToString(), offset, RecordLimit, Authentication, TableName);
                                        List<string> res = EloquaData.Split(',').ToList();
                                        int totalResults = Convert.ToInt32(res[0].Split(':')[1].ToString());//{"totalResults":0
                                        if (totalResults == 0)
                                        {
                                            string urlval = dr["SyncUrl"].ToString() + "/data?offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                            balMain.insertTransationRecord(TableName, InstanceId, "Transation", urlval, "Inserted = 0");
                                        }
                                        else
                                        {
                                            if (EloquaData == null || EloquaData == "null" || EloquaData == "NULL" || EloquaData == "")
                                            {
                                                if (s <= 2)
                                                {
                                                    if (s == 2)
                                                    {
                                                        string urlval = dr["SyncUrl"].ToString() + "/data?offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                                        balMain.insertTransationRecord(TableName, InstanceId, "Error", urlval, "Null Data");
                                                    }
                                                    mm -= 1;
                                                }
                                                s += 1;
                                            }
                                            else
                                            {
                                                s = 0;
                                                dsExport.Tables.Add(JsonConvert.DeserializeObject<DataTable>(JsonConvert.DeserializeObject<dynamic>(EloquaData).items.ToString()));

                                                if (TableName != "contacts")
                                                    dsExport.Tables[mm].TableName = datasettablename[mm]; //dsExport.Tables[cnt - 1].TableName = "Subscribe";//dr["TableName"].ToString();

                                                var rowcount = dsExport.Tables[0].Rows.Count;
                                                var columncount = dsExport.Tables[0].Columns.Count;

                                                for (int r = 0; r < rowcount; r++)
                                                {
                                                    for (int c = 0; c < columncount; c++)
                                                    {
                                                        if (dsExport.Tables[0].Rows[r][c].ToString() == "") { dsExport.Tables[0].Rows[r][c] = null; }
                                                    }
                                                }
                                                string resultval = "";
                                                if (TableName == "accounts")
                                                    resultval = balMain.Insert_Update_Accounts(dsExport.Tables[0], InstanceId);
                                                else resultval = balMain.Insert_Activities(dsExport.Tables[0], InstanceId, TableName);

                                                if (resultval.ToLower().Contains(logname.ToLower())) //Maduraiveeran
                                                {
                                                    //balMain.SendEmail();
                                                    Environment.Exit(0);
                                                    //High Priority Mail Send *******************//
                                                }
                                                else
                                                {
                                                    int inserted = 0;
                                                    if (resultval == "") { totalinsert += 0; }
                                                    else
                                                    {
                                                        inserted = Convert.ToInt32(resultval);
                                                        totalinsert += Convert.ToInt32(resultval);
                                                    }
                                                    string url = dtExportUrl.Rows[0][1].ToString() + "/data?offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                                    balMain.insertTransationRecord(TableName, InstanceId, "Transation", url, "Object Rerun Inserted = " + inserted);

                                                    dsExport.Dispose();
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        string url = dtExportUrl.Rows[0][1].ToString() + "/data?offset=" + offset.ToString() + "&limit=" + RecordLimit.ToString();
                                        balMain.insertTransationRecord(TableName, InstanceId, "Error", url, e.ToString());
                                    }
                                }
                            }
                            balMain.insertrecordcount(TableName, RecordCount, totalinsert, InstanceId); //To insert Total records
                        }
                    }
                }
                balMain.InsertObjectHistory(InstanceId, TableId, "ExpEnd");  //Export End Record Insert End
            }//Else Part End
        }

        // Start Contact Temp Table and insert procedure for old instance
        public void CreateContact_TempTable_and_Procedure(int InstanceId, string InstanceName)
        {
            try
            {
                string typetable1 = ""; string typetable2 = ""; string typetable3 = ""; string typetable4 = ""; int i = 0;
                string DbwithTableName = dbname + "..Contact";
                string stringsp1 = "";
                string stringsp4 = "";
                string typename = "";
                ///// $$$$$s for typetable 5
                string typetable5 = "";
                ///// $$$$$e
                ///// $$$$$$s xml
                string xmltable = "";

                ///// $$$$$e

                string proc1 = ",@new_identity INT = NULL OUTPUT ) AS BEGIN BEGIN TRY Declare @RecordCount Table (Cnt varchar(100))   BEGIN TRANSACTION T1  ";
                string str1 = ""; string typetablename1 = ""; string typetablename2 = ""; string typetablename3 = ""; string typetablename4 = "";
                ///// $$$$$s
                string typetablename5 = "";
                ///// $$$$$e
                ///// $$$$$s xml
                string xmltempteble = "";// " inner join XMLContactTempTable TEMP ON TEMP.ContactId = F.ContactID";
                ///// $$$$$e

                string str3 = "ContactId=S_Table.ContactId, ";
                DataTable dt = balMain.getTablecolumnFieldsforinstance("contacts", Convert.ToString(InstanceId));
                ///// $$$$$s drop contact table fields
                //int affected = balMain.DroptableinContacts("contacts", InstanceId);
                ///// $$$$$e
                if (dt.Rows.Count > 0)
                {
                    typetable1 += "CREATE TABLE [dbo].[Contact_First_" + InstanceName + "] ( [ContactId] [int] NOT NULL PRIMARY KEY,";
                    typetable2 += "CREATE TABLE [dbo].[Contact_Second_" + InstanceName + "] ( [ContactId] [int] NOT NULL PRIMARY KEY,";
                    typetable3 += "CREATE TABLE [dbo].[Contact_Third_" + InstanceName + "]  ( [ContactId] [int] NOT NULL PRIMARY KEY,";
                    typetable4 += "CREATE TABLE [dbo].[Contact_Fourth_" + InstanceName + "] ( [ContactId] [int] NOT NULL PRIMARY KEY,";
                    ///// $$$$$s
                    typetable5 += "CREATE TABLE [dbo].[Contact_Fifth_" + InstanceName + "] ([ContactId] [int] NOT NULL PRIMARY KEY,";
                    ///// $$$$$e
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (i > 0)
                        {
                            string fieldname = dr["FieldName"].ToString();
                            stringsp1 += fieldname + ",";
                            stringsp4 += "S_Table." + fieldname + " ,";
                            str3 += fieldname + " = S_Table." + fieldname + " ,";
                            if (i <= 99)
                            {
                                typetable1 += "[" + fieldname + "]  [nvarchar](max) NULL,";
                                str1 += "F." + fieldname + ",";
                                typetablename1 = "Contact_First_" + InstanceName + " F  ";
                            }

                            if (i > 99 && i <= 198)
                            {
                                typetable2 += "[" + fieldname + "]  [nvarchar](max) NULL,";
                                str1 += "S." + fieldname + ",";
                                typetablename2 = "  inner join  Contact_Second_" + InstanceName + " S  ON S.ContactID = F.ContactID ";
                            }

                            if (i > 198 && i <= 297)
                            {
                                typetable3 += "[" + fieldname + "]  [nvarchar](max) NULL,";
                                str1 += "T." + fieldname + ",";
                                typetablename3 = "  inner join Contact_Third_" + InstanceName + "  T  ON T.ContactID = F.ContactID  ";
                            }

                            if (i > 297 && i <= 396)
                            {
                                typetable4 += "[" + fieldname + "]  [nvarchar](max) NULL,";
                                str1 += "Fr." + fieldname + ",";
                                typetablename4 = "  inner join  Contact_Fourth_" + InstanceName + " Fr  ON Fr.ContactID = F.ContactID  ";
                            }
                            ///// $$$$$s this is for extra fields
                            if (i > 396 && i < 495)
                            {
                                typetable5 += "[" + fieldname + "] [nvarchar](max) NULL,";
                                str1 += "Fi." + fieldname + ",";
                                typetablename5 = "inner join Contact_Fifth_" + InstanceName + " Fi ON Fi.ContactID = F.ContactID  ";
                            }
                            ///// $$$$$e
                        }
                        i += 1;
                    }
                }

                if (typetable1.Count() > 100)
                {
                    typetable1 = typetable1.TrimEnd(',') + ")";
                    balMain.creeatecontact_temptable("Contact_First_" + InstanceName + "", typetable1);
                    typename += " Contact_First_" + InstanceName + " F ";
                }
                if (typetable2.Count() > 100)
                {
                    typetable2 = typetable2.TrimEnd(',') + ")";
                    balMain.creeatecontact_temptable("Contact_Second_" + InstanceName + "", typetable2);
                    typename += " INNER JOIN Contact_Second_" + InstanceName + " S ON F.ContactId = S.ContactId ";
                }
                if (typetable3.Count() > 100)
                {
                    typetable3 = typetable3.TrimEnd(',') + ")";
                    balMain.creeatecontact_temptable("Contact_Third_" + InstanceName + "", typetable3);
                    typename += " INNER JOIN Contact_Third_" + InstanceName + "  T ON T.ContactId = F.ContactId ";
                }
                if (typetable4.Count() > 100)
                {
                    typetable4 = typetable4.TrimEnd(',') + ")";
                    balMain.creeatecontact_temptable("Contact_Fourth_" + InstanceName + "", typetable4);
                    typename += " INNER JOIN Contact_Fourth_" + InstanceName + " Fr ON Fr.ContactId = F.ContactId ";
                }
                ///// $$$$$s for table 5
                if (typetable5.Count() > 100)
                {
                    typetable5 = typetable5.TrimEnd(',') + ")";
                    balMain.creeatecontact_temptable("Contact_Fifth_" + InstanceName + "", typetable5);
                    typename += "  INNER JOIN Contact_Fifth_" + InstanceName + " Fi ON Fi.contactId = F.ContactId";
                }
                ///// $$$$$e

                //string finalstr = "CREATE PROCEDURE [dbo].[USP_Insert_Update_Contacts_" + InstanceName + "_Test](@InstanceId INT , @FirstPart Type_Contact_First_" + InstanceName + " READONLY , @SecondPart Type_Contact_Second_" + InstanceName + " READONLY";
                string finalstr = "CREATE PROCEDURE [dbo].[USP_Insert_Update_Contacts_" + InstanceName + "_insert](@InstanceId INT ";

                string tempinsertsp = "Create PROCEDURE [dbo].[USP_Insert_Contacts_" + InstanceName + "_Temp]  (@Type varchar(100), @InstanceId INT, @FirstPart Type_Contact_First_" + InstanceName + " READONLY ";
                // $$$$$s xml changes
                //string tempinsertsp = "Create PROCEDURE [dbo].[USP_Insert_Contacts_" + InstanceName + "_Temp]  (@Type varchar(100), @InstanceId INT, @XmlContact Type_Contact_Xml READONLY, @FirstPart Type_Contact_First_" + InstanceName + " READONLY ";
                // $$$$$e

                var insertstmt = " INSERT INTO EloquaConfig..Contact_First_" + InstanceName + " Select * from @FirstPart";

                // &&&&&s xml
                //insertstmt += "  INSERT INTO XMLContactTempTable Select * from @XmlContact";
                // $$$$$e

                if (typetable2.Count() > 100)
                {
                    tempinsertsp += ", @SecondPart Type_Contact_Second_" + InstanceName + " READONLY";
                    insertstmt += " INSERT INTO EloquaConfig..Contact_Second_" + InstanceName + " Select * from @SecondPart";
                }

                if (typetable3.Count() > 100)
                {
                    //finalstr += ",@ThirdPart Type_Contact_Third_" + InstanceName + " READONLY ";
                    tempinsertsp += ", @ThirdPart Type_Contact_Third_" + InstanceName + " READONLY ";
                    insertstmt += " INSERT INTO EloquaConfig..Contact_Third_" + InstanceName + " Select * from @ThirdPart";
                }
                if (typetable4.Count() > 100)
                {
                    //finalstr += ",@Fourth Type_Contact_Fourth_" + InstanceName + " READONLY";
                    tempinsertsp += ", @Fourth Type_Contact_Fourth_" + InstanceName + " READONLY ";
                    insertstmt += " INSERT INTO EloquaConfig..Contact_Fourth_" + InstanceName + " Select * from @FourthPart";
                }
                ///// $$$$$s for fifth table count greater than 100
                if (typetable5.Count() > 100)
                {
                    tempinsertsp += ", @Fifth Type_Contact_Fifth_" + InstanceName + " READONLY ";
                    insertstmt += " INSERT INTO EloquaConfig..Contact_Fifth_" + InstanceName + " Select * from @FifthPart";
                }
                ///// $$$$$e

                tempinsertsp += ",@new_identity INT = NULL OUTPUT) AS BEGIN " + insertstmt + " SET @new_identity = @@ROWCOUNT SELECT @new_identity END";

                //tempinsert sp for insert temp table

                //proc1 +=" ;MERGE  " + DbwithTableName + " AS T_TABLE  Using ( select F.ContactId," + str1 + " @InstanceId InstanceId, GETDATE() CreatedDate,''  UpdatedDate from " + typetablename1 + " " + typetablename2 + " " + typetablename3 + " " + typetablename4 + " " + typetablename5 +  "   ) as S_Table ON S_Table.ContactId=T_Table.ContactId  and T_Table.InstanceId=@InstanceId ";
                //proc1 += " When MATCHED then Update set  " + str3 + " InstanceId = @InstanceId, UpdatedDate = GETDATE()";
                //proc1 += " When Not MATCHED Then Insert (ContactId, " + stringsp1 + " InstanceId, CreatedDate, UpdatedDate ) values (S_Table.ContactId, " + stringsp4 + " @InstanceId, GETDATE(), '') ";
                //string trycatcherr = " OUTPUT $action into @RecordCount; SET @new_identity = (select COUNT(*) from @RecordCount)   SELECT @new_identity   COMMIT TRANSACTION T1   END TRY  BEGIN CATCH IF @@TRANCOUNT > 0  ROLLBACK TRANSACTION T1  INSERT INTO EloquaConfig..TransactionLog (InstanceId,TableName,LogType,Mssg,LogDate)";
                //trycatcherr += " VALUES (@InstanceId,'Contact','DataBase Err',ERROR_MESSAGE(),GETDATE())   SET @new_identity = ERROR_MESSAGE()  SELECT @new_identity  END CATCH  end";
                ////$$$$
                //proc1 += " ;MERGE  " + DbwithTableName + " AS T_TABLE  Using ( select F.ContactId," + str1 + " @InstanceId InstanceId, GETDATE() CreatedDate,''  UpdatedDate, CAST(CAST (TEMP.xmlcolumn AS VARCHAR(MAX)) AS XML) AS xmlcolumn from " + typetablename1 + " " + xmltempteble + " " + typetablename2 + " " + typetablename3 + " " + typetablename4 + " " + typetablename5 + "   ) as S_Table ON S_Table.ContactId=T_Table.ContactId  and T_Table.InstanceId=@InstanceId ";
                ///$$$$
                proc1 += " ;MERGE  " + DbwithTableName + " AS T_TABLE  Using ( select F.ContactId," + str1 + " @InstanceId InstanceId, GETDATE() CreatedDate,''  UpdatedDate  from " + typetablename1 + " " + xmltempteble + " " + typetablename2 + " " + typetablename3 + " " + typetablename4 + " " + typetablename5 + "   ) as S_Table ON S_Table.ContactId=T_Table.ContactId  and T_Table.InstanceId=@InstanceId ";
                ///$$$$madur
                //proc1 += " When MATCHED then Update set  " + str3 + " InstanceId = @InstanceId, UpdatedDate = GETDATE(),XmlColumn = CAST(CAST (S_Table.xmlcolumn AS VARCHAR(MAX)) AS XML) ";
                ////$$$$
                proc1 += " When MATCHED then Update set  " + str3 + " InstanceId = @InstanceId, UpdatedDate = GETDATE() ";
                ///$$$madurai
                //proc1 += " When Not MATCHED Then Insert (ContactId, " + stringsp1 + " InstanceId, CreatedDate, UpdatedDate,XmlColumn  ) values (S_Table.ContactId, " + stringsp4 + " @InstanceId, GETDATE(), '',CAST(CAST (S_Table.xmlcolumn AS VARCHAR(MAX)) AS XML)) ";
                //$$$madurai
                proc1 += " When Not MATCHED Then Insert (ContactId, " + stringsp1 + " InstanceId, CreatedDate, UpdatedDate) values (S_Table.ContactId, " + stringsp4 + " @InstanceId, GETDATE(), '') ";
                string trycatcherr = " OUTPUT $action into @RecordCount; SET @new_identity = (select COUNT(*) from @RecordCount)   SELECT @new_identity   COMMIT TRANSACTION T1   END TRY  BEGIN CATCH IF @@TRANCOUNT > 0  ROLLBACK TRANSACTION T1  INSERT INTO EloquaConfig..TransactionLog (InstanceId,TableName,LogType,Mssg,LogDate)";
                trycatcherr += " VALUES (@InstanceId,'Contact','DataBase Err',ERROR_MESSAGE(),GETDATE())   SET @new_identity = ERROR_MESSAGE()  SELECT @new_identity  END CATCH  end";

                finalstr = finalstr + proc1 + " " + trycatcherr;
                balMain.CreateContactTempprocedure(finalstr, "USP_Insert_Update_Contacts_" + InstanceName + "_insert", tempinsertsp, "USP_Insert_Contacts_" + InstanceName + "_Temp");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //Console.ReadLine();
            }
        }

        //
    }
}