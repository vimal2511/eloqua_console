﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Net;
using System.IO;
using System.Configuration;
using System.Threading;
using System.Data;
using System.Xml;
using DAL;
using BAL;
using System.Net.Mail;
using System.Data.SqlClient;

namespace BAL
{
    public class BAL_Main
    {
        private DAL_Main EloquaConfigCon = new DAL_Main("EloquaConfigCon");
        public static string Fields, AccountFields, prefixcolumn;
        private DataTable dt = new DataTable();
        private DataSet ds = new DataSet();

        public DataTable Get_Instancename(string Instancename)
        {
            DAL_Main Getinstance = new DAL_Main("EloquaConfigCon");
            Getinstance.AddParameter("@Instancename", Instancename);
            ds = Getinstance.ExecuteDataSet("SP_Read_Instance");
            dt = ds.Tables[0];
            return dt;
        }

        public string update_TableFields_contact(int InstanceId)
        {
            try
            {
                DAL_Main tablefields = new DAL_Main("EloquaConfigCon");
                tablefields.AddParameter("@InstanceId", InstanceId);
                tablefields.ExecuteNonQuery("SP_Update_TableFields_Contact");
            }
            catch (Exception e)
            {
                string Errorstring = "Insert Activities Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return "";
        }

        // $$$$$s delete values in xmltemptable
        public void DeleteInXMLContactTempTable()
        {
            DAL_Main deleterecords = new DAL_Main("EloquaConfigCon");

            try
            {
                deleterecords.ExecuteDataSet("SP_DeleteInXMLContactTempTable");
            }
            catch (Exception e)
            {
                string Errorstring = "Deleted Record Count Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
        }

        public DataSet GetXMLList(string Instanceid, string TableName)
        {
            DAL_Main addcolumn = new DAL_Main("EloquaConfigCon");

            try
            {
                addcolumn.AddParameter("@InstanceId", Instanceid);
                addcolumn.AddParameter("@TableName", TableName);
                ds = addcolumn.ExecuteDataSet("GetXMLLIst");
            }
            catch (Exception e)
            {
                string Errorstring = "Get Instance Table Column Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return ds;
        }

        // $$$$$e xml
        public DataSet getTablecolumnFields(int loop, string TableName, int InstanceId, int LastRunInstanceId)
        {
            DAL_Main GetTableColumn = new DAL_Main("EloquaConfigCon");
            try
            {
                GetTableColumn.AddParameter("@TableName", TableName);
                GetTableColumn.AddParameter("@loop", loop);
                GetTableColumn.AddParameter("@InstanceId", InstanceId);
                GetTableColumn.AddParameter("@LastRunInstanceId", LastRunInstanceId);
                ds = GetTableColumn.ExecuteDataSet("SP_GetTableColumnFields");
            }
            catch (Exception e)
            {
                string Errorstring = "Get Table Column Fileds Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return ds;
        }

        public DataSet GetCustomObjectandFields(int InstanceId)
        {
            try
            {
                DAL_Main Custobjectfields = new DAL_Main("EloquaConfigCon"); //Changed
                Custobjectfields.AddParameter("@InstanceId", InstanceId);
                ds = Custobjectfields.ExecuteDataSet("SP_GetCustomObjectandFields");
            }
            catch (Exception e)
            {
                string Errorstring = "Get ExportUrl Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return ds;
        }

        public DataTable GetCustomobjectdatatopid(int InstanceId)
        {
            DAL_Main GetEntityid = new DAL_Main("EloquaConfigCon"); //changed
            try
            {
                GetEntityid.AddParameter("@InstanceId", InstanceId);
                ds = GetEntityid.ExecuteDataSet("SP_GetCustomObjectdatatopId");
                dt = ds.Tables[0];
            }
            catch (Exception e)
            {
                string Errorstring = "Get CustomObjectData Top ID Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return dt;
        }

        public string Insert_CustomObjectData(DataTable dtCustomObjectdata, int InstanceId)
        {
            DAL_Main insertcustom = new DAL_Main("EloquaConfigCon"); //changed
            DataSet data = new DataSet();
            try
            {
                insertcustom.AddParameter("@InstanceId", InstanceId);
                insertcustom.AddParameter("@CustomObjectData", dtCustomObjectdata);
                data = insertcustom.ExecuteDataSet("USP_Insert_CustomObjectData");
            }
            catch (Exception e)
            {
                string Errorstring = "Insert CustomObjectData Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return data.Tables[0].Rows[0][0].ToString();
        }

        /// <summary>
        /// /////// $$$$$s this is for droping the columns in contacts table
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public int DroptableinContacts(string tablenames, int instancesid)
        {
            int affected = 0;
            DAL_Main GetTableColumn = new DAL_Main("EloquaConfigCon");
            try
            {
                GetTableColumn.AddParameter("@TableNames", tablenames);
                GetTableColumn.AddParameter("@instanceid", instancesid);
                affected = Convert.ToInt32(GetTableColumn.ExecuteScalar("SP_DropColumnsinContacts"));
            }
            catch (Exception e)
            {
                string Errorstring = "Get Table Column Fileds Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return affected;
        }

        public int GetRecordLimit(string TableName)
        {
            DAL_Main getRecordLimit = new DAL_Main("EloquaConfigCon");
            int limit = 0;
            try
            {
                getRecordLimit.AddParameter("@TableName", TableName);
                ds = getRecordLimit.ExecuteDataSet("SP_GetRecordLimitCount");
                limit = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
            }
            catch (Exception e)
            {
                string Errorstring = "Get RecordLimit Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return limit;
        }

        public string Insert_Update_Contacts(DataSet ds, int InstanceId, DataTable xmldt)
        {
            DAL_Main insertcontact = new DAL_Main("EloquaConfigCon"); //changed
            DAL_Main oldins = new DAL_Main("EloquaConfigCon");
            DataSet res = new DataSet();
            try
            {
                DataSet data = CheckNewInstancestauts(InstanceId);
                bool oldinstancestatus = Convert.ToBoolean(data.Tables[0].Rows[0]["NewInstance"].ToString());
                if (oldinstancestatus == false)
                {
                    insertcontact.AddParameter("@Type", "New");
                }
                else
                {
                    insertcontact.AddParameter("@Type", "Update");
                }
                insertcontact.AddParameter("@InstanceId", InstanceId);
                insertcontact.AddParameter("@FirstPart", ds.Tables[0]);

                int totaltable = ds.Tables.Count;

                if (totaltable >= 2)
                {
                    insertcontact.AddParameter("@SecondPart", ds.Tables[1]);
                }
                if (totaltable >= 3)
                {
                    insertcontact.AddParameter("@ThirdPart", ds.Tables[2]);
                }
                if (totaltable >= 4)
                {
                    insertcontact.AddParameter("@FourthPart", ds.Tables[3]);
                }
                ///// $$$$$s i have added this
                if (totaltable >= 5)
                {
                    insertcontact.AddParameter("@FifthPart", ds.Tables[4]);
                }

                //insertcontact.AddParameter("@XmlContact", xmldt);
                ///// $$$$$e

                oldins.AddParameter("@Instanceid", InstanceId);
                DataSet ds1 = oldins.ExecuteDataSet("SP_GetPartcularinstanceid");
                int instid = Convert.ToInt32(ds1.Tables[0].Rows[0]["InstanceId"].ToString());
                string spname = "USP_Insert_Update_Contacts"; // "ICIS"- Default
                spname = "USP_Insert_Update_Contacts_" + ds1.Tables[0].Rows[0]["InstanceName"].ToString();
                if (oldinstancestatus == true)
                {
                    spname = "USP_Insert_Contacts_" + ds1.Tables[0].Rows[0]["InstanceName"].ToString() + "_Temp";
                    res = insertcontact.ExecuteDataSet(spname);
                }
                else
                {
                    res = insertcontact.ExecuteDataSet(spname);
                }
            }
            catch (Exception e)
            {
                string Errorstring = "Insert Contact Table Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return res.Tables[0].Rows[0][0].ToString();
        }

        public string Insert_Update_Accounts(DataTable ds, int InstanceId)
        {
            DAL_Main insertupdateaccount = new DAL_Main("EloquaConfigCon"); //changed
            DAL_Main configacc = new DAL_Main("EloquaConfigCon");
            DataSet res = new DataSet();
            try
            {
                DataSet data = CheckNewInstancestauts(InstanceId);
                bool newinstancestatus = Convert.ToBoolean(data.Tables[0].Rows[0]["NewInstance"].ToString());
                if (newinstancestatus == false)
                {
                    insertupdateaccount.AddParameter("@Type", "New");
                }
                else
                {
                    insertupdateaccount.AddParameter("@Type", "Update");
                }
                insertupdateaccount.AddParameter("@InstanceId", InstanceId);
                insertupdateaccount.AddParameter("@Account", ds);//ds.Tables[0]
                configacc.AddParameter("@Instanceid", InstanceId);
                DataSet ds1 = configacc.ExecuteDataSet("SP_GetPartcularinstanceid");
                string spname = "USP_Add_Update_Account"; // "ICIS"- Default
                int instid = Convert.ToInt32(ds1.Tables[0].Rows[0]["InstanceId"].ToString());
                spname = "USP_Add_Update_Account_" + ds1.Tables[0].Rows[0]["InstanceName"].ToString();
                res = insertupdateaccount.ExecuteDataSet(spname);
            }
            catch (Exception e)
            {
                string Errorstring = "Insert Account Table Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return res.Tables[0].Rows[0][0].ToString();
        }

        public string Insert_Activities(DataTable ds, int InstanceId, string TableName)
        {
            DAL_Main insert = new DAL_Main("EloquaConfigCon");
            DAL_Main formfield = new DAL_Main("EloquaConfigCon");
            DataSet res = new DataSet();
            try
            {
                insert.AddParameter("@InstanceId", InstanceId);
                insert.AddParameter("@" + TableName, ds);//ds.Tables[TableName]
                DataSet dt = new DataSet();
                if (TableName == "FormSubmit")
                {
                    formfield.AddParameter("@InstanceId", InstanceId);
                    formfield.AddParameter("@" + TableName, ds);
                    formfield.ExecuteNonQuery("USP_Insert_FormField");
                }
                res = insert.ExecuteDataSet("USP_Insert_" + TableName);
            }
            catch (Exception e)
            {
                string Errorstring = "Insert Activities Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return res.Tables[0].Rows[0][0].ToString();
        }

        public string GetContactfieldsjson(int loop, string TableName, int InstanceId, int ErrorlogID, int newinstancesplit_month) //ErrorLogID add only for rerun object error
        {
            DataTable dt = new DataTable();
            DataSet datas = new DataSet();
            var currentdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");//ToString("yyyy-M-d HH:mm:ss"); //2015-04-05 11:48:01
            //var currentdate = currentdate1.Split(' ')[0];// +" 00:00:00";
            try
            {
                if (TableName != "contacts" && TableName != "accounts")
                {
                    datas = getTablecolumnFields(loop, TableName, 1, InstanceId);
                }
                else
                {
                    datas = getTablecolumnFields(loop, TableName, InstanceId, InstanceId);
                }
                dt = datas.Tables[0];
                var yesterday = "";//System.DateTime.Now.AddDays(-2).ToString("yyyy-M-d HH:mm:ss");
                var lastrundate = "";
                if (datas.Tables[1].Rows.Count > 0)
                {
                    lastrundate = datas.Tables[1].Rows[0]["LastRunDate"].ToString();
                }

                switch (ErrorlogID)
                {
                    case 0:
                        if (lastrundate != "")
                        {
                            var yesterdy = datas.Tables[1].Rows[0]["LastRunDate"].ToString().Replace('/', '-');
                            // var yrsdy = yesterdy.Split(' ')[0] + " 00:00:00";
                            // yesterday = Convert.ToDateTime(yrsdy).ToString("yyyy-MM-dd");//ToString("yyyy-MM-dd HH':'mm':'ss");
                            var date1 = Convert.ToDateTime(yesterdy).ToString("yyyy-MM-dd HH:mm:ss");//ToString("yyyy-MM-dd HH':'mm':'ss");
                            var date2 = Convert.ToDateTime(date1).Add(new TimeSpan(-7, 0, 0));
                            yesterday = date2.ToString("yyyy-MM-dd HH:mm:ss");
                        }
                        else
                        {
                            var yesterday1 = System.DateTime.Now.AddDays(-2).ToString("yyyy-MM-dd HH':'mm':'ss");
                            var yrsdy = yesterday1.Split(' ')[0] + " 00:00:00";
                            //yesterday = Convert.ToDateTime(yrsdy).ToString("yyyy-MM-dd");//.ToString("MM/dd/yyyy");//.ToString("yyyy-MM-dd HH':'mm':'ss");
                            yesterday = Convert.ToDateTime(yesterday1).ToString("yyyy-MM-dd HH : mm : ss");
                        }
                        break;

                    default:
                        DataSet errds = getobjecterrorrundate(ErrorlogID);
                        var dat = errds.Tables[0].Rows[0][0].ToString();
                        yesterday = Convert.ToDateTime(dat).ToString("yyyy-MM-dd HH : mm : ss");
                        break;
                }
                //yesterday = System.DateTime.Now.AddDays(-2).ToString("yyyy-M-d HH:mm:ss");
                if (loop > 1 && TableName == "contacts")
                {
                    Fields = "{\"name\":\"Merit " + TableName + " Export - " + DateTime.Now.ToString("dd-MM-yyyy") + "\",\"fields\":{" + prefixcolumn;
                }
                else
                {
                    Fields = "{\"name\":\"Merit " + TableName + " Export - " + DateTime.Now.ToString("dd-MM-yyyy") + "\",\"fields\":{";
                }
                if (dt.Rows.Count > 0)
                {
                    // Fields += "{";
                    int i = 1;
                    if (TableName == "accounts")
                    { dt.Rows.Add(1, "AccountId", "Account.Id", "accounts", "", 1, 1); }
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (i < dt.Rows.Count)
                        {
                            if (dr["Prefix"].ToString().Length == 0)
                            {
                                Fields = Fields + "\"" + dr["FieldName"].ToString() + "\":\"{{" + dr["EloquaFieldName"].ToString() + "}}\",";
                                if (i == 1) { prefixcolumn = "\"" + dr["FieldName"].ToString() + "\":\"{{" + dr["EloquaFieldName"].ToString() + "}}\","; }
                            }
                            else
                            {
                                Fields = Fields + "\"" + dr["FieldName"].ToString() + "\":\"{{" + dr["Prefix"].ToString() + "(" + dr["EloquaFieldName"].ToString() + ")}}\",";
                            }
                        }
                        else
                        {
                            if (dr["Prefix"].ToString().Length == 0)
                            {
                                Fields = Fields + "\"" + dr["FieldName"].ToString() + "\":\"{{" + dr["EloquaFieldName"].ToString() + "}}\"}";
                                if (i == 1)
                                {
                                    prefixcolumn = "\"" + dr["FieldName"].ToString() + "\":\"{{" + dr["EloquaFieldName"].ToString() + "}}\",";
                                }
                            }
                            else
                            {
                                Fields = Fields + "\"" + dr["FieldName"].ToString() + "\":\"{{" + dr["Prefix"].ToString() + "(" + dr["EloquaFieldName"].ToString() + ")}}\"}";
                            }
                        }
                        i++;
                    }
                }
                ds = CheckNewInstancestauts(InstanceId);
                bool newinstancestatus = Convert.ToBoolean(ds.Tables[0].Rows[0]["NewInstance"].ToString());

                #region Command Line

                //if (newinstancestatus == true && TableName == "contacts") {  Fields += ","+"\"filter\":\"'{{Contact.Field(C_DateCreated)}}'>='" + currentdate + "' OR '{{Contact.Field(C_DateModified)}}'>='" + currentdate + "'\"}"; }
                //else if (newinstancestatus == true && TableName == "accounts") { Fields += "," + "\"filter\":\"'{{Account.Field(M_DateCreated)}}'>='" + currentdate + "' OR '{{Account.Field(M_DateModified)}}'>='" + currentdate + "'\"}"; }
                //else if (newinstancestatus == true && TableName != "contacts" && TableName != "accounts") { Fields += "," + "\"filter\":\"'{{Activity.Type}}'='" + TableName + "'  AND '{{Activity.CreatedAt}}'>='" + currentdate + "'\"}"; }
                //else if (TableName != "contacts" && TableName != "accounts") { Fields += "," + "\"filter\":\"'{{Activity.Type}}'='" + TableName + "'\"}"; }
                //else { Fields += "}"; }

                #endregion Command Line

                //yesterday = Convert.ToDateTime("2015-12-31 10:12:12").ToString("yyyy-MM-dd  HH':'mm':'ss");//Convert.ToDateTime("2014-07-01").ToString("yyyy-MM-dd");
                //currentdate = Convert.ToDateTime("2016-01-31").ToString("yyyy-MM-dd  HH':'mm':'ss");
                if (newinstancesplit_month == 1)
                { yesterday = Convert.ToDateTime(yesterday).AddMonths(-3).ToString("yyyy-MM-dd HH : mm : ss"); }

                Fields += "," + "\"autoDeleteDuration\":\"PT12H\""; //This is delete the api records after 12 hours 26-03-2018
                if (newinstancestatus == true || newinstancesplit_month == 1)
                {
                    switch (TableName)
                    {
                        case "contacts":
                            //Fields += "," + "\"filter\":\"(('{{Contact.Field(C_DateCreated)}}' >= '" + yesterday + "' AND '{{Contact.Field(C_DateCreated)}}' <= '" + currentdate + "')  OR ('{{Contact.Field(C_DateModified)}}' >= '" + yesterday + "' AND '{{Contact.Field(C_DateModified)}}' <= '" + currentdate + "'))\"}";
                            Fields += "," + "\"filter\":\"('{{Contact.Field(C_DateCreated)}}' >= '" + yesterday + "' OR '{{Contact.Field(C_DateModified)}}' >= '" + yesterday + "')\"}";
                            break;

                        case "accounts":
                            //Fields += "," + "\"filter\":\"('{{Account.Field(M_DateCreated)}}' >= '" + yesterday + "' AND '{{Account.Field(M_DateCreated)}}' < '" + currentdate + "') OR ('{{Account.Field(M_DateModified)}}' >= '" + yesterday + "' AND '{{Account.Field(M_DateModified)}}' < '" + currentdate + "')\"}";
                            Fields += "," + "\"filter\":\"('{{Account.Field(M_DateCreated)}}' >= '" + yesterday + "' OR '{{Account.Field(M_DateModified)}}' >= '" + yesterday + "')\"}";
                            break;

                        case "FormSubmit":
                        case "formsubmit":
                            //Fields += "," + "\"filter\":\"'{{Activity.Type}}'='" + TableName + "' and ('{{Activity.CreatedAt}}'>='" + yesterday + "' and '{{Activity.CreatedAt}}'<'" + currentdate + "')  and not '{{Activity.Asset.Name}}' ~: 'CWM%' \"}";
                            //Fields += "," + "\"filter\":\"'{{Activity.Type}}'='" + TableName + "' and '{{Activity.CreatedAt}}'>='" + yesterday + "' and not '{{Activity.Asset.Name}}' ~: 'CWM%' \"}"; //To avoid Assert Name start with CWM
                            Fields += "," + "\"filter\":\"'{{Activity.Type}}'='" + TableName + "' and '{{Activity.CreatedAt}}'>='" + yesterday + "' and not '{{Activity.Asset.Name}}' ~: 'CWM%' and not '{{Activity.Asset.Name}}' ~: '%ignore%' \"}"; //To avoid Assert Name start with CWM

                            break;

                        case "WebVisit":
                        case "Webvisit":
                        case "webvisit":
                            Fields += "," + "\"filter\":\"'{{Activity.Type}}'='" + TableName + "'  AND '{{Activity.CreatedAt}}'>'" + yesterday + "' AND '{{Activity.CreatedAt}}'<'" + currentdate + "'\"}";

                            break;

                        default:
                            Fields += "," + "\"filter\":\"'{{Activity.Type}}'='" + TableName + "'  AND '{{Activity.CreatedAt}}'>='" + yesterday + "' AND '{{Activity.CreatedAt}}'<'" + currentdate + "'\"}";
                            break;
                    }
                }
                else if (TableName.ToLower() == "FormSubmit".ToLower())
                {
                    Fields += "," + "\"filter\":\"'{{Activity.Type}}'='" + TableName + "' and not '{{Activity.Asset.Name}}' ~: 'CWM%' and not '{{Activity.Asset.Name}}' ~: '%ignore%' \"}"; //To avoid Assert Name start with CWM
                }
                else if (TableName != "contacts" && TableName != "accounts")
                {
                    Fields += "," + "\"filter\":\"'{{Activity.Type}}'='" + TableName + "'\"}";
                }
                else
                {
                    Fields += "}";
                }
            }
            catch (Exception e)
            {
                string Errorstring = "Create Json File Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return Fields;
        }

        public string GetCustomObjectfieldsJson(string Name, DataTable dtFields, int InstanceId)
        {
            Fields = "";
            DataTable dt = new DataTable();
            var CustomObjectId = 0;
            try
            {
                string lastrundate = System.DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd HH:mm:ss");  //2015-04-05 11:48:01
                int i = 1;
                Fields = "{\"name\":\"CustomObject - " + Name + " Export - " + DateTime.Now.ToString("ddMMyyyy") + "\",\"fields\": {";
                foreach (DataRow dr in dtFields.Rows)
                {
                    if (i < dtFields.Rows.Count)
                        Fields = Fields + "\"" + dr["internalName"].ToString() + "\":\"" + dr["statement"].ToString() + "\",";
                    else
                        Fields = Fields + "\"" + dr["internalName"].ToString() + "\":\"" + dr["statement"].ToString() + "\"";
                    i++;
                    CustomObjectId = Convert.ToInt32(dr["CustomObjectID"].ToString());
                }
                ds = CheckNewInstancestauts(InstanceId);
                bool oldinstancestatus = Convert.ToBoolean(ds.Tables[0].Rows[0]["NewInstance"].ToString());
                string lastrundate1 = ds.Tables[1].Rows[0]["LastRunDate"].ToString();
                var lastdate1 = lastrundate1.Split(' ');
                var lastdate = lastdate1[0].Replace("/", "-");
                var date = lastdate.Split('-');
                Fields += "} ";
                if (oldinstancestatus == true)
                {
                    var rundate = date[2] + "-" + date[1] + "-" + date[0] + " " + lastdate1[1];
                    var date1 = Convert.ToDateTime(rundate).ToString("yyyy-MM-dd HH:mm:ss");
                    var date2 = Convert.ToDateTime(date1).Add(new TimeSpan(-7, 0, 0));
                    lastrundate = date2.ToString("yyyy-MM-dd HH:mm:ss");
                    Fields += "," + "\"autoDeleteDuration\":\"PT12H\""; //This is delete the api records after 12 hours 26-03-2018
                    Fields += "," + "\"filter\":\"'{{CustomObject[" + CustomObjectId + "].CreatedAt}}'>='" + lastrundate + "' or '{{CustomObject[" + CustomObjectId + "].UpdatedAt}}'>='" + lastrundate + "'\"";
                }
                // +"," + "\"filter\":\"'{{updatedAt}}'>='" + currentdate + "'\""; //,\"updatedAt\":\'" + currentdate + "'\""; //
            }
            catch (Exception e)
            {
                string Errorstring = "Create CustomObjectFiled Json Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return Fields = Fields + "}";
        }

        public string getCustomObjectFields(DataTable dtFields)
        {
            DataTable dt = new DataTable();
            var currentdate = System.DateTime.Now.AddDays(-1).ToString("yyyy-M-d HH:mm:ss");  //2015-04-05 11:48:01
            int i = 1; string Fields = ""; ;
            foreach (DataRow dr in dtFields.Rows)
            {
                if (i < dtFields.Rows.Count)
                    Fields = Fields + "\"" + dr["internalName"].ToString() + "\":\"" + dr["statement"].ToString() + "\",";
                else
                    Fields = Fields + "\"" + dr["internalName"].ToString() + "\":\"" + dr["statement"].ToString() + "\"";
                i++;
            }
            Fields += "AND '{{Activity.CreatedAt}}'>='" + currentdate;
            return Fields = Fields + "}"; ;
        }

        public DataSet CheckNewInstancestauts(int InstanceId)
        {
            DAL_Main checkinstancestatus = new DAL_Main("EloquaConfigCon");
            checkinstancestatus.AddParameter("@InstanceId", InstanceId);
            return ds = checkinstancestatus.ExecuteDataSet("SP_CheckInstanceStatus");
        }

        public DataSet GetTableFiekdName(string TableName, int InstanceId)
        {
            DAL_Main getFieldName = new DAL_Main("EloquaConfigCon");
            getFieldName.AddParameter("@TableName", TableName);
            getFieldName.AddParameter("@InstanceId", InstanceId);
            return ds = getFieldName.ExecuteDataSet("SP_GetTableFieldName");
        }

        public void altercolumnname(string TableName, string newFieldName, string oldFieldName, int InstanceId)
        {
            DAL_Main altertablecolumn = new DAL_Main("EloquaConfigCon");
            altertablecolumn.AddParameter("@TableName", TableName);
            altertablecolumn.AddParameter("@oldFieldName", oldFieldName);
            altertablecolumn.AddParameter("@newFieldName", newFieldName);
            altertablecolumn.AddParameter("@InstanceId", InstanceId);
            altertablecolumn.ExecuteDataSet("SP_UpdateTableFieldName");
        }

        public void CreateAlterColumn(string TableName, string ColumnName, string Columnrename, string Type)
        {
            try
            {
                DAL_Main addcolumn = new DAL_Main("EloquaConfigCon");
                addcolumn.AddParameter("@TableName", TableName);
                addcolumn.AddParameter("@ColumnName", ColumnName);
                addcolumn.AddParameter("@Columnrename", Columnrename);
                addcolumn.AddParameter("@Type", Type);
                addcolumn.ExecuteDataSet("USP_Create_Alter_Table");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public DataTable getTablecolumnFieldsforinstance(string TableName, string InstanceId)
        {
            DAL_Main addcolumn = new DAL_Main("EloquaConfigCon");
            try
            {
                addcolumn.AddParameter("@TableName", TableName);
                addcolumn.AddParameter("@InstanceId", InstanceId);
                ds = addcolumn.ExecuteDataSet("SP_GetInstanceTableColumn");
            }
            catch (Exception e)
            {
                string Errorstring = "Get Instance Table Column Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return ds.Tables[0];
        }

        public void creeateTypetable(string TypeTableName, string TypeTable, string procedurename, string proceduretempname)
        {
            try
            {
                DAL_Main addtypetable = new DAL_Main("EloquaConfigCon"); //changed
                addtypetable.AddParameter("@TypeTableName", TypeTableName);
                addtypetable.AddParameter("@TypeTable", TypeTable);
                addtypetable.AddParameter("@procedurename", procedurename);
                addtypetable.AddParameter("@proceduretempname", proceduretempname);
                addtypetable.ExecuteDataSet("SP_CreateTypeTable");
            }
            catch (Exception e)
            {
                string Errorstring = "Create Type Table Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
        }

        public void Createprocedure(string finalstr, string procedurename)
        {
            try
            {
                DAL_Main createproc = new DAL_Main("EloquaConfigCon"); //changed
                createproc.AddParameter("@procedurename", procedurename);
                createproc.AddParameter("@procedure", finalstr);
                createproc.ExecuteDataSet("SP_Createprocedure");
            }
            catch (Exception e)
            {
                string Errorstring = "Create Procedure Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
        }

        public void Insert_TableFields(DataSet ds, int InstanceId, string TableName)
        {
            try
            {
                DAL_Main addtablefields = new DAL_Main("EloquaConfigCon");
                addtablefields.AddParameter("@InstanceId", InstanceId);
                addtablefields.AddParameter("@TableName", TableName);
                addtablefields.AddParameter("@TableFields", ds.Tables["TableFields"]);
                addtablefields.ExecuteNonQuery("SP_Insert_TableFields");
            }
            catch (Exception e)
            {
                string Errorstring = "Insert Table Fields Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
        }

        public DataSet GetInstnaceslist()
        {
            DAL_Main newinstance = new DAL_Main("EloquaConfigCon");
            return ds = newinstance.ExecuteDataSet("SP_GetInstanceslist");
        }

        public string InsertCustomObject(DataTable dtCustomObject, int InstanceId)
        {
            DAL_Main insertcustobj = new DAL_Main("EloquaConfigCon"); //changed
            DataSet data = new DataSet();
            try
            {
                insertcustobj.AddParameter("@InstanceId", InstanceId);
                insertcustobj.AddParameter("@CustomObject", dtCustomObject);
                data = insertcustobj.ExecuteDataSet("USP_Insert_CustomObject");
            }
            catch (Exception e)
            {
                string Errorstring = "Insert CustomObject Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return data.Tables[0].Rows[0][0].ToString();
        }

        public DataTable SelectCustomObjects(int InstanceId)
        {
            DAL_Main selectcusobjs = new DAL_Main("EloquaConfigCon"); //changed
            try
            {
                selectcusobjs.AddParameter("@InstanceId", InstanceId);
                ds = selectcusobjs.ExecuteDataSet("SP_GetCustomobjects");
            }
            catch (Exception e)
            {
                string Errorstring = "Select CustomObjects Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return ds.Tables[0];
        }

        public string Insert_SelectCustomObjectFields(DataTable dtFormField, int InstanceId)
        {
            DAL_Main insertcustobjfields = new DAL_Main("EloquaConfigCon"); //changed
            DataSet data = new DataSet();
            try
            {
                insertcustobjfields.AddParameter("@InstanceId", InstanceId);
                insertcustobjfields.AddParameter("@CustomObjectField", dtFormField);
                data = insertcustobjfields.ExecuteDataSet("USP_Insert_CustomObjectFields");
            }
            catch (Exception e)
            {
                string Errorstring = "Insert CustomObject Fields Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return data.Tables[0].Rows[0][0].ToString();
        }

        public void InsertInstanceHistory(int InstanceId, string Type)
        {
            try
            {
                DAL_Main insertinstance = new DAL_Main("EloquaConfigCon");
                insertinstance.AddParameter("@InstanceId", InstanceId);
                insertinstance.AddParameter("@Type", Type);
                insertinstance.ExecuteNonQuery("sp_InsertInstanceHistory");
            }
            catch (Exception e)
            {
                string Errorstring = "Insert Instance History Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
        }

        public void InsertObjectHistory(int InstanceId, int TableId, string Type)
        {
            try
            {
                DAL_Main insertinstancehis = new DAL_Main("EloquaConfigCon");
                insertinstancehis.AddParameter("@IntanceId", InstanceId);
                insertinstancehis.AddParameter("@ObjectId", TableId);
                insertinstancehis.AddParameter("@Type", Type);
                insertinstancehis.ExecuteNonQuery("SP_InsertObjectHistory");
            }
            catch (Exception e)
            {
                string Errorstring = "Insert Object History Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
        }

        public void InsertObjectHistoryRerun(int InstanceId, int TableId, string Type)
        {
            try
            {
                DAL_Main insertinstancehis = new DAL_Main("EloquaConfigCon");
                insertinstancehis.AddParameter("@IntanceId", InstanceId);
                insertinstancehis.AddParameter("@ObjectId", TableId);
                insertinstancehis.AddParameter("@Type", Type);
                insertinstancehis.ExecuteNonQuery("USP_InsertObjectHistoryReRun");
            }
            catch (Exception e)
            {
                string Errorstring = "Insert CustomObjectHistory ReRun Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
        }

        public void insertrecordcount(string TableName, int RecordCount, int totalinsert, int InstanceId)
        {
            DAL_Main updateins = new DAL_Main("EloquaConfigCon");//changed
            try
            {
                updateins.AddParameter("@TableName", TableName);
                updateins.AddParameter("@EloquaTotalRecord", RecordCount);
                updateins.AddParameter("@InsertedRecord", totalinsert);
                updateins.AddParameter("@InstanceId", InstanceId);
                updateins.ExecuteNonQuery("sp_insertcount");
            }
            catch (Exception e)
            {
                string Errorstring = "Insert Record Count Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
        }

        public void insertTransationRecord(string TableName, int InstanceId, string LogType, string Url, string Mssg)
        {
            DAL_Main translog = new DAL_Main("EloquaConfigCon");
            try
            {
                translog.AddParameter("@TableName", TableName);
                translog.AddParameter("@InstanceId", InstanceId);
                translog.AddParameter("@LogType", LogType);
                translog.AddParameter("@Url", Url);
                translog.AddParameter("@Mssg", Mssg);
                translog.ExecuteNonQuery("USP_InsertTransationRecords");
            }
            catch (Exception e)
            {
                string Errorstring = "Insert Transaction Record Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
        }

        public int GetObjectId(string TableName)
        {
            DAL_Main objectid = new DAL_Main("EloquaConfigCon");
            try
            {
                objectid.AddParameter("@TableName", TableName);
                ds = objectid.ExecuteDataSet("USP_GetObjectId");
            }
            catch (Exception e)
            {
                string Errorstring = "Get Object Id Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return Convert.ToInt32(ds.Tables[0].Rows[0]["ObjectId"].ToString());
        }

        public int TableColumncount(string TableName, int InstanceId)
        {
            DAL_Main objectid = new DAL_Main("EloquaConfigCon");
            try
            {
                objectid.AddParameter("@TableName", TableName);
                objectid.AddParameter("@InstanceId", InstanceId);
                ds = objectid.ExecuteDataSet("USP_GetTableColumncount");
            }
            catch (Exception e)
            {
                string Errorstring = "Get Table Column Count Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
        }

        public void InsertCustomObjectsyncstatus(DateTime sysncstartime, DateTime sysnendtime, DateTime exprtstartTime, DateTime exportendtime, int CustomobjectRecordCount, int InsertedRecord, int InstanceId)
        {
            DAL_Main insertcustomstatus = new DAL_Main("EloquaConfigCon");
            try
            {
                insertcustomstatus.AddParameter("@InstanceId", InstanceId);
                insertcustomstatus.AddParameter("@sysncstartime", sysncstartime.ToString("HH:mm:ss"));
                insertcustomstatus.AddParameter("@sysnendtime", sysnendtime.ToString("HH:mm:ss"));
                insertcustomstatus.AddParameter("@exprtstartTime", exprtstartTime.ToString("HH:mm:ss"));
                insertcustomstatus.AddParameter("@exportendtime", exportendtime.ToString("HH:mm:ss"));
                insertcustomstatus.AddParameter("@CustomobjectRecordCount", CustomobjectRecordCount);
                insertcustomstatus.AddParameter("@InsertedRecord", InsertedRecord);
                insertcustomstatus.ExecuteDataSet("USP_InsertCustomObjectHistory");
            }
            catch (Exception e)
            {
                string Errorstring = "Insert Object current status Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
        }

        public void UpdateTransactionLogTable(int Id, int InstanceId)
        {
            DAL_Main updatestatus = new DAL_Main("EloquaConfigCon");
            try
            {
                updatestatus.AddParameter("@Id", Id);
                updatestatus.AddParameter("@InstanceId", InstanceId);
                updatestatus.ExecuteDataSet("USP_UpdateErrorRerunstatus");
            }
            catch (Exception e)
            {
                string Errorstring = "Update TransactionLog Table Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
        }

        public DataTable selectrerunurlslist(string type)
        {
            DAL_Main errlist = new DAL_Main("EloquaConfigCon");
            try
            {
                errlist.AddParameter("@type", type);
                ds = errlist.ExecuteDataSet("USP_SelectRerunUrls1");
                dt = ds.Tables[0];
            }
            catch (Exception e)
            {
                string Errorstring = "Select Rerun url list Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return dt;
        }

        public DataSet GetCustomObjectandFieldsRerunurl(int InstanceId, string CustomObjectId)
        {
            DAL_Main eloquacustomerrurls = new DAL_Main("EloquaConfigCon");
            try
            {
                eloquacustomerrurls.AddParameter("@InstanceId", InstanceId);
                eloquacustomerrurls.AddParameter("@CustomObjectId", CustomObjectId);
                ds = eloquacustomerrurls.ExecuteDataSet("USP_Selectcustomobjecterrs");
            }
            catch (Exception e)
            {
                string Errorstring = "Get CustomObject Error list Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return ds;
        }

        public DataTable GetInstanceIdandTableId(string InstanceName, string TableName)
        {
            DAL_Main getinstanceandtableid = new DAL_Main("EloquaConfigCon");
            try
            {
                getinstanceandtableid.AddParameter("@InstanceName", InstanceName);
                getinstanceandtableid.AddParameter("@ObjectName", TableName);
                ds = getinstanceandtableid.ExecuteDataSet("USP_selectInstanceId_TableId");
            }
            catch (Exception e)
            {
                string Errorstring = "Get Instance and Table ID Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return ds.Tables[0];
        }

        public DataSet getobjecterrorrundate(int ErrorlogID)
        {
            DAL_Main rundate = new DAL_Main("EloquaConfigCon");
            try
            {
                rundate.AddParameter("@ErrorlogID", ErrorlogID);
                ds = rundate.ExecuteDataSet("USP_Geterrorobjectrundate");
            }
            catch (Exception e)
            {
                string Errorstring = "Get Object Error Update Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return ds;
        }

        public void creeatecontact_temptable(string TableName, string query)
        {
            DAL_Main addtypetable = new DAL_Main("EloquaConfigCon"); //changed
            try
            {
                addtypetable.AddParameter("@TableName", TableName);
                addtypetable.AddParameter("@query", query);
                addtypetable.ExecuteDataSet("USP_Create_Conctact_TempTable");
            }
            catch (Exception e)
            {
                string Errorstring = "Create Contact Temp Table Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
        }

        public void CreateContactTempprocedure(string finalstr, string procedurename, string tempinsert, string tempinsertprocname)
        {
            DAL_Main createproc = new DAL_Main("EloquaConfigCon"); //changed
            try
            {
                createproc.AddParameter("@procedurename", procedurename);
                createproc.AddParameter("@procedure", finalstr);
                createproc.AddParameter("@tempinsertprocname", tempinsertprocname);
                createproc.AddParameter("@tempinsert", tempinsert);
                createproc.ExecuteDataSet("SP_Createprocedure_For_Contact");
            }
            catch (Exception e)
            {
                string Errorstring = "Create Contact Temp Table Create Procedure Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
        }

        public void insertcontacttemp_To_ContactTable(int InstanceId, string InstanceName)
        {
            DAL_Main insertandorptable = new DAL_Main("EloquaConfigCon");
            try
            {
                insertandorptable.AddParameter("@InstanceId", InstanceId);
                insertandorptable.AddParameter("@storedprocname", "USP_Insert_Update_Contacts_" + InstanceName + "_insert");
                insertandorptable.AddParameter("@table1", "Contact_First_" + InstanceName);
                insertandorptable.AddParameter("@table2", "Contact_Second_" + InstanceName);
                insertandorptable.AddParameter("@table3", "Contact_Third_" + InstanceName);
                insertandorptable.AddParameter("@table4", "Contact_Fourth_" + InstanceName);
                insertandorptable.ExecuteDataSet("USP_Insert_Contact_Droptemp_Table");
            }
            catch (Exception e)
            {
                string Errorstring = "Insert Contact Temp Table Insert Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
        }

        public void updateTotalrecordinsert(int Id, string TableName, int InstanceId, int insertedcount)
        {
            DAL_Main updaterecordcount = new DAL_Main("EloquaConfigCon"); //changed
            try
            {
                updaterecordcount.AddParameter("@translogId", Id);
                updaterecordcount.AddParameter("@TableName", TableName);
                updaterecordcount.AddParameter("@InstanceId", InstanceId);
                updaterecordcount.AddParameter("@insertedcount", insertedcount);
                updaterecordcount.ExecuteDataSet("USP_Update_RecountTable_Object");
            }
            catch (Exception e)
            {
                string Errorstring = "Update Inserted Record Count Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
        }

        public DataSet deletenullfromFormSubmit()
        {
            DAL_Main deletenull = new DAL_Main("EloquaConfigCon");
            try
            {
                ds = deletenull.ExecuteDataSet("USP_DeleteNullValue_FormSubmit");
            }
            catch (Exception e)
            {
                string Errorstring = "Clear Null value in Formsubmit Object Error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
            return ds;
        }

        //delete dupilcate records
        public void Delete_duplicaterecordsall()
        {
            try
            {
                DAL_Main insertinstance = new DAL_Main("EloquaConfigCon");
                insertinstance.ExecuteNonQuery("USP_Clear_Duplicate_Records_All");
            }
            catch (Exception e)
            {
                string Errorstring = "Duplicate delete time error : " + Convert.ToString(e);
                WriteErrorlog_TextFile(Errorstring);
            }
        }

        #region Write Text File for Error Log File

        public void WriteErrorlog_TextFile(string errstring)
        {
            try
            {
                var url = @"D:\\Eloqua_DET App\\Errorlog.txt";//@"D:\Eloqua_DET Config\Errorlog.txt"; //This is live Error Log file
                StreamWriter objWriter = new StreamWriter(url, false);
                objWriter.WriteLine(errstring.ToString());
                objWriter.Close();

                //TextWriter tw = new StreamWriter("date.txt");
                //tw.WriteLine(DateTime.Now);
                //File.AppendAllText("date.txt", DateTime.Now.ToString() + Environment.NewLine);
                //tw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error For Write Error log Text File");
                Console.WriteLine(e);
            }
        }

        #endregion Write Text File for Error Log File

        #region Command Line Functions

        //public DataSet GetNewInstnace()
        //   {
        //         DAL_Main newinstance = new DAL_Main("EloquaConfigCon");
        //         return ds = newinstance.ExecuteDataSet("SP_GetNewInstance");
        //   }
        //public void UpdateInstancestatus(int InstanceId)
        //   {
        //       try
        //       {
        //           DAL_Main updateins = new DAL_Main("EloquaConfigCon");
        //           updateins.AddParameter("@InstanceId", InstanceId);
        //           updateins.ExecuteNonQuery("SP_UpdateInstancestatus");
        //       }
        //         catch (Exception e)
        //         {
        //             string Errorstring = "Update Instance Status Error : " + Convert.ToString(e);
        //             WriteErrorlog_TextFile(Errorstring);
        //         }
        //   }
        //public DataTable GetLastInstanceTableName()
        //{
        //    DAL_Main getLastinstancename = new DAL_Main("EloquaConfigCon");
        //    ds = getLastinstancename.ExecuteDataSet("SP_GetlatestinstanceName");
        //    return ds.Tables[0];
        //}
        //public DataTable Get_ExportUrl(string TableName)
        //{
        //    DAL_Main GetExporturl = new DAL_Main("EloquaConfigCon");
        //    try
        //    {
        //        GetExporturl.AddParameter("@TableName", TableName);
        //        ds = GetExporturl.ExecuteDataSet("SP_GetExporturlandsyncurl");
        //        dt = ds.Tables[0];
        //    }
        //    catch (Exception e)
        //    {
        //        string Errorstring = "Get ExportUrl Error : " + Convert.ToString(e);
        //        WriteErrorlog_TextFile(Errorstring);
        //    }
        //    return dt;
        //}
        //public DataTable Get_RawData(int InstanceId)
        //{
        //    DAL_Main getdata = new DAL_Main("EloquaConfigCon");//changed
        //    try
        //    {
        //        getdata.AddParameter("@InstanceId", InstanceId);
        //        ds = getdata.ExecuteDataSet("USP_SelectFormSubmit");
        //    }
        //    catch (Exception e)
        //    {
        //        string Errorstring = "Get RawData Error : " + Convert.ToString(e);
        //        WriteErrorlog_TextFile(Errorstring);
        //    }
        //    return ds.Tables[0];
        //}
        //public void Insert_FormField(DataTable dtFormField, int InstanceId)
        //{
        //    //DAL_Main forminsert = new DAL_Main("EloquaConfigCon");//changed
        //    //forminsert.AddParameter("@InstanceId", InstanceId);
        //    //forminsert.AddParameter("@FormField", dtFormField);
        //    //forminsert.ExecuteNonQuery("USP_Insert_FormField");
        //}
        //public DataSet GetTableName()
        //{
        //    DAL_Main EloquaConfigCon = new DAL_Main("EloquaConfigCon");
        //    ds = EloquaConfigCon.ExecuteDataSet("SP_GetTableName");
        //    return ds;
        //}

        //public void funcall()
        //{
        //    string stringval = "";
        //    DAL_Main EloquaConfigCon = new DAL_Main("EloquaConfigCon"); //changed
        //    EloquaConfigCon.Executesql("select FieldName from TableFields where TableName='contacts'  and InstanceId=1 and LoopCount=3", dtExportUrl, "");
        //    var count = dtExportUrl.Rows.Count;

        //    if (dtExportUrl.Rows.Count > 0)
        //    { foreach (DataRow dr in dtExportUrl.Rows)
        //        { stringval += " T." + dr["FieldName"].ToString()+" , ";
        //        }
        //    }
        //    var ss = stringval;
        //}

        #endregion Command Line Functions

        #region Email Send Function

        public void SendEmail()//This is send mail for Log Table Issue
        {
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            message.To.Add("maduraiveeran.raja@meritgroup.co.uk");
            message.Subject = "SQL Server Log Table Is Full";
            message.From = new System.Net.Mail.MailAddress("maduraiveeran.raja@gmail.com");//diamond@rbi.co.uk
            message.Body = "Please Clear the Log Table";
            SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
            smtp.Credentials = new System.Net.NetworkCredential("maduraiveeran.raja@gmail.com", "M_123456789");
            smtp.EnableSsl = true;

            try
            {
                smtp.Send(message); //smtp.Send(mail);
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine(e);
                throw;
            }
            catch (ObjectDisposedException e)
            {
                Console.WriteLine(e);
                throw;
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e);
                throw;
            }
            catch (SmtpFailedRecipientsException e)
            {
                Console.WriteLine(e);
                throw;
            }
            catch (SmtpException e)
            {
                // I get an SmtpException error
                Console.WriteLine(e);
                throw;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        #endregion Email Send Function
    }
}