﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;
using System.Configuration;
using System.Threading;
using System.Data;
using System.Xml;
using EAL;
using BAL;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Diagnostics;

using System;

using System.ServiceProcess;

namespace Eloqua_DET
{
    internal class Program
    {
        public static BAL_Main balMain = new BAL_Main();
        public static DataTable dt = new DataTable();
        public static DataSet ds = new DataSet();
        public static string constring = "";//"rbiqhssqlt014v#~#rbi_det#~#El0quad#t#~#EloquaConfig";

        private static void Main(string[] args1)
        {  // string[] args = Environment.GetCommandLineArgs();
            string name = "Normal";
            string InstanceName = "ICIS";
            string TableName = "accounts";

            try
            {
                name = args1[0];
                Console.WriteLine(name);
                if (name == "Table")
                {
                    InstanceName = args1[1];
                    TableName = args1[2];
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error : " + e);
            }

            try
            {
                string url = "http://10.55.50.117:8080/database_elsepart.txt"; //"http://10.55.50.117:8080/database.txt";//"http://172.27.139.121/Eloqua/database.txt";//@"D:\Maduraiveeran\Projects\Svn\connection\database.txt";//
                var client = new WebClient();
                var text = client.DownloadString(url);
                var base64EncodedBytes = System.Convert.FromBase64String(text);
                var constring = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                UpdateAppSettings_KeyValues(constring.Replace("Merit", ""));
            }
            catch (Exception e)
            {
                // constring = "rbiqhssqlt014v#~#rbi_det#~#El0quad#t#~#EloquaConfig#~#MEDM";
                var path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\DBConnction.txt"; //"DBConnction.txt";//"D:\\Eloqua_DET App\\Release\\DBConnction.txt";//
                constring = System.IO.File.ReadAllText(@path);
                UpdateAppSettings_KeyValues(constring);
                balMain.WriteErrorlog_TextFile(Convert.ToString(e));
            }

            //balMain.SendEmail();
            try
            {
                switch (name)
                {
                    case "Manual":
                        Console.WriteLine("Manual Function Call");
                        MenaulRerunfunction();
                        //Console.WriteLine("Particular Error Table ReRun");
                        // MenaulRerunpartcularTableReRun();
                        break;

                    case "Table":
                        Console.WriteLine("Manual Call Particular Table");
                        EAL_Main ealMain1 = new EAL_Main(InstanceName);
                        DataTable idval = balMain.GetInstanceIdandTableId(InstanceName, TableName);
                        int InstanceId = Convert.ToInt16(idval.Rows[0][0].ToString());
                        int TableId = Convert.ToInt16(idval.Rows[1][0].ToString());
                        if (TableName == "CustomObjectData") ealMain1.Create_CustomObjectExport(InstanceId);
                        else { ealMain1.ExportFromEloqua(TableName, InstanceId, InstanceName, TableId); }
                        break;

                    default:
                        Console.WriteLine("Normal Function Call");
                        NormalFunction();
                        balMain.deletenullfromFormSubmit(); //This function is delete null value from FormField Object
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Connection Error.... Please Check Connection  String");
                Console.WriteLine(e);
                balMain.WriteErrorlog_TextFile(Convert.ToString(e));
            }
        }

        public static void NormalFunction()
        {
            try
            {
                DataSet ds2 = balMain.GetInstnaceslist();

                for (int k = 0; k < ds2.Tables[0].Rows.Count; k++) //
                {
                    int InstanceId = Convert.ToInt32(ds2.Tables[0].Rows[k]["InstanceId"].ToString());
                    string InstanceName = ds2.Tables[0].Rows[k]["InstanceName"].ToString();
                    bool newinstance = Convert.ToBoolean(ds2.Tables[0].Rows[k]["NewInstance"].ToString());
                    EAL_Main ealMain1 = new EAL_Main(InstanceName);
                    var tablerowcount = ds2.Tables[1].Rows.Count;
                    ealMain1.changeeloquaapiname(InstanceId);
                    //if(k!=4)
                    balMain.InsertInstanceHistory(InstanceId, "New"); //Instance Status Process

                    List<String> list = new List<String>();
                    for (int i = 0; i < tablerowcount; i++) //
                    //for (int i = 2; i < 3; i++) //
                    {
                        string TableName = ds2.Tables[1].Rows[i]["ObjectName"].ToString();
                        int TableId = Convert.ToInt16(ds2.Tables[1].Rows[i]["ObjectId"].ToString());

                        if (TableName == "contacts" || TableName == "accounts")//
                        {
                            balMain.update_TableFields_contact(InstanceId);//update table fields loopvalues
                            // balMain.DroptableinContacts("contacts", InstanceId);// this is drop column from contact table
                            //if (newinstance == false)
                            //{
                            //    //This is removed for avoid column issue
                            //ealMain1.GetFields(TableName, InstanceId); //Add new fields  add for TableFields
                            //ealMain1.CreateTypetableVariable(TableName, InstanceId, InstanceName);// Create TypeTable and Procedure
                            //}
                            // int totalTablecolumncount = ealMain1.GetFieldscount(TableName, InstanceId);
                            //if (totalTablecolumncount == 0)
                            //{
                            //ealMain1.GetFields(TableName, InstanceId); //Add new fields  add for TableFields
                            ealMain1.CreateTypetableVariable(TableName, InstanceId, InstanceName);// Create TypeTable and Procedure
                            //}
                        }
                        if (TableName == "CustomObjectData")
                        {
                            ealMain1.Get_CustomObjects(InstanceId);  //Insert CustomObjects
                            ealMain1.Insert_CustomObjectFields(InstanceId); //Insert CustomObject Fields
                            ealMain1.Create_CustomObjectExport(InstanceId);  //Insert CustomObject table fields
                        }
                        else
                            ealMain1.ExportFromEloqua(TableName, InstanceId, InstanceName, TableId);
                    }

                    balMain.InsertInstanceHistory(InstanceId, "Update"); //Update instance status and InstanceHistory Status Update
                }
                balMain.Delete_duplicaterecordsall();//Delete duplicate records in email open, email send, formsubmit
            }
            catch (Exception e)
            {
                //Console.WriteLine(e);
                //Console.ReadLine();
            }
        }

        public static void MenaulRerunfunction()
        {
            string type = "";
            DataTable dt = balMain.selectrerunurlslist(type);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int InstanceId = Convert.ToInt32(dt.Rows[i]["InstanceId"].ToString());

                    string InstanceName = dt.Rows[i]["InstanceName"].ToString();
                    EAL_Main ealMain1 = new EAL_Main(InstanceName);
                    ealMain1.changeeloquaapiname(InstanceId);
                    int RerunId = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
                    string TableName = dt.Rows[i]["TableName"].ToString();
                    string Url = dt.Rows[i]["Url"].ToString();
                    ealMain1.ExportFromEloquamanual(RerunId, InstanceId, TableName, Url, InstanceName);
                }
            }
        }

        #region Manaul ReRun Partcular Table ReRun

        public static void MenaulRerunpartcularTableReRun()
        {
            string type = "Object";
            DataTable dt = balMain.selectrerunurlslist(type);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    int InstanceId = Convert.ToInt32(dt.Rows[i]["InstanceId"].ToString());
                    string InstanceName = dt.Rows[i]["InstanceName"].ToString();
                    int RerunId = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
                    string TableName = dt.Rows[i]["TableName"].ToString();
                    if (TableName == "CustomObject") { TableName = "CustomObjectData"; }
                    int TableID = Convert.ToInt32(dt.Rows[i]["TableID"].ToString());
                    string Url = dt.Rows[i]["Url"].ToString();
                    EAL_Main ealMain1 = new EAL_Main(InstanceName);
                    ealMain1.ExportFromEloquapartTable(TableName, InstanceId, TableID, Url, RerunId);
                    balMain.UpdateTransactionLogTable(RerunId, InstanceId);
                }
            }
        }

        #endregion Manaul ReRun Partcular Table ReRun

        #region Update App Setting Key Values

        public static void UpdateAppSettings_KeyValues(string newValue)
        {
            newValue = newValue.Replace("Merit", "");

            //user id=User2;password=Merit456;data source=172.27.137.181;initial catalog=EloquaConfig;
            //rbiqhssqlt014v#~#Meritrbi_det#~#MeritEl0quad#t#~#EloquaConfig#~#MEDM
            //if(newValue.Trim() != "rbiqhssqlt014v#~#rbi_det#~#El0quad#t#~#EloquaConfig")
            if (newValue.Trim() != "rbiqhssqlt014v#~#rbi_det#~#El0quad#t1#~#EloquaConfig#~#MEDM")
            {
                constring = newValue;
                var url = "D:\\Eloqua_DET App\\Release\\DBConnction.txt";//@"DBConnction.txt"; //This is live Error Log file
                StreamWriter objWriter = new StreamWriter(url, false);
                objWriter.WriteLine(constring);
                objWriter.Close();
            }
            var loginlist = Regex.Split(newValue, "#~#");
            var servername = loginlist[0];
            var username = loginlist[1];
            var password = loginlist[2];
            var SP_databasename = loginlist[3];
            var databasename = loginlist[4];
            EAL_Main el = new EAL_Main("1"); //statically put instance id 1
            el.changedatasename(databasename);
            var keyvalue = "user id=" + username + ";password=" + password + ";data source=" + servername + ";initial catalog=EloquaConfig;";
            try
            {
                Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                configuration.AppSettings.Settings["EloquaConfigCon"].Value = keyvalue;
                // configuration.AppSettings.Settings["EloquaDataCon"].Value = keyvalue;
                configuration.Save();
                ConfigurationManager.RefreshSection("appSettings");
            }
            catch (Exception e)
            {
                balMain.WriteErrorlog_TextFile("Update Connection String for App.config :" + Convert.ToString(e));
                Console.WriteLine(e);
            }
        }

        #endregion Update App Setting Key Values

        #region Email Send Function

        //public static void SendMailMessage()
        //{
        //    const string subject = "TESTING";
        //    const string body = "";

        //    string from = "maduraiveeran.merit@gmail.com";
        //    string to = "maduraiveeran.raja@gmail.com";
        //    var message = new MailMessage(from, to, subject, body);

        //    var smptClient = new SmtpClient("smtp.gmail.com", 587)
        //    {
        //        Credentials = new NetworkCredential("news.takkarr@gmail.com", "valiant123"),
        //        EnableSsl = true
        //    };
        //    smptClient.Send("news.takkarr@gmail.com", "maduraiveeran.raja@gmail.com", "Testing Email", "testing the email");

        //}

        //public static string SendEmail()
        //{
        //    string returnval = string.Empty;
        //    try
        //    {
        //        string Mailbody = "<br/>";
        //        Mailbody += "Dear Admin ! <br/><br/> You have recieved Log Information from Eloqua Database<br/><br/>";
        //        Mailbody += "<asp:Panel id='panel1' runat='server' CssClass='Panelstyle'>";
        //        Mailbody += "<table border='2' cellspacing='1' frame='box' rules='all' style='width: 900px;'><thead style='background-color: #006DCC; color: white;'><tr>";
        //        Mailbody += "<th style='border-right:1px solid white;padding:5px;'>S.No</th><th style='border-right:1px solid white;'>Table Name</th><th style='border-right:1px solid white;'>Iteration Number</th><th style='border-right:1px solid white;'>Off Set Value</th>";
        //        //Mailbody += "<th style='border-right:1px solid white;'>Email Id</th><th style='border-right:1px solid white;'>Ticket Rate</th><th style='border-right:1px solid white;'>No.of Tickets</th><th style='border-right:1px solid white;'>Total Price</th>";
        //        Mailbody += "</tr></thead><tbody style='background-color: #EEEEEE;'>";
        //        MailMessage mail = new MailMessage();
        //        Mailbody += "<tr><td></td><td></td><td></td><td></td></tr>";
        //        // Mailbody += "<tr><td colspan='6' align='right'>Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td></td><td>$.00</td></tr>";
        //        Mailbody += "</tbody></table><asp:Panel/>";
        //        //SendMailMessage("maduraiveeran.raja@gmail.com", "maduraiveeran.raja@gmail.com", "", "", "Eloqua Database Table Log information", Mailbody);
        //        SendMailMessage("maduraiveeran.merit@gmail.com", "maduraiveeran.raja@gmail.com", "", "", "Eloqua Database Table Log information", Mailbody);
        //        returnval = "success";
        //    }
        //    catch { returnval = "fail"; }
        //    return returnval;
        //}
        //public static string SendMailMessage(string from, string to, string bcc, string cc, string subject, string body)
        //{
        //    try
        //    { //string host = ConfigurationManager.AppSettings["host1"].ToString();
        //        string adminmailid = "maduraiveeran.merit@gmail.com";//"maduraiveeran.raja@meritgroup.co.uk";//"news.takkarr@gmail.com";
        //        string adminpass = "madurai123";// "M$dur2iv3r";//"valiant123";
        //        MailMessage mail = new MailMessage();
        //        mail.From = new System.Net.Mail.MailAddress(from);
        //        mail.To.Add(new MailAddress(to));
        //        if (!(string.IsNullOrEmpty(cc))) { mail.CC.Add(new MailAddress(cc)); }
        //        mail.Subject = subject;
        //        mail.IsBodyHtml = true;
        //        mail.Body = body;
        //        // System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();smtp.Host = host; smtp.Credentials = new System.Net.NetworkCredential(adminmailid, adminpass);smtp.SendAsyncCancel();
        //        string host = "smtp.gmail.com";//"smtp.meritgroup.co.uk";
        //        var smtp = new SmtpClient { Host = host, Port = 587, EnableSsl = true, DeliveryMethod = SmtpDeliveryMethod.Network, UseDefaultCredentials = false, Credentials = new NetworkCredential(adminmailid, adminpass) };
        //        smtp.Send(mail);

        //    }
        //    catch (Exception ex) { }

        //    //MailMessage mail = new MailMessage(); mail.Subject = subject; mail.From = new MailAddress("maduraiveeran.raja@gmail.com");
        //    //mail.To.Add("maduraiveeran.muthu10@gmail.com"); mail.Body = body; mail.IsBodyHtml = true; SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587); smtp.EnableSsl = true;
        //    //NetworkCredential netCre = new NetworkCredential("maduraiveeran.raja@gmail.com", "muthurakkammal2010"); smtp.Credentials = netCre;
        //    //try { smtp.Send(mail); }
        //    //catch (Exception ex) { }

        //    return "success";
        //}

        #endregion Email Send Function
    }
}